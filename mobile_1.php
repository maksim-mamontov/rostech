<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link href="/img/favicon.png" rel="shortcut icon" type="image/png" />

<meta content='True' name='HandheldFriendly' />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

<title>Тачь стол мобильный</title>
<link type="text/css" rel="stylesheet" media="all" href="css/table-style.css">
<!-- libs -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/b4w.min.js"></script>
<!-- *libs -->
<script src="js/tt-text-content.js"></script>
<script src="js/tt-global-data.js"></script>
<script src="js/tt-function.js"></script>

<script>
	createPage = false; // Включить выключить автоматическое создание страници
	rotaryL = false; // включение переворачивания блоков после линии
	view3dObjectName = true; // отображает имя 3Д объекта в сценe ( если мы на него кликнули ) 
	kinetic = false; // Физика движений для блоков, включить выключить.
	adminControlWindow = false; // включить панель управления администратора
</script>

</head>
<!--<body  onmousedown="return false" onselectstart="return false">-->
<body>
<div class="disable-touch">
<div class="commercial disable-touch">
<div class="content" style="width:100%; height:100%;">

<div class="block block-object" style="top:0; left:0; width:100%; height:100%; border:none; ">
	<div class="block-rotary" style="transform: rotate(0deg);">
		<div class="blok-content">
			<div class="canvas3d" id="canvas3d_2t">
				<div class="preloader3d" style="display: block;"><p><span style="width: 100%;"></span></p><h4>100%</h4></div>
			</div>
		</div>
	</div>
</div>

<div class="ttm-central-but" style="transform: rotate(0deg);"></div>

<div class="ttm-all-mini-but">
	<div class="ttm-all-mini-butC">
		<div class="ttm-mini-but" data-cont3d="3D/ez/Diag_EZ7.json"><div class="shine mini-but-s"></div></div>
		<div class="ttm-mini-but" data-cont3d="3D/ug/Diag_UG6.json"><div class="shine mini-but-s"></div></div>
		<div class="ttm-mini-but" data-cont3d="3D/asu/Diag_ASU3_color.json"><div class="shine mini-but-s"></div></div>
		<div class="ttm-mini-but" data-cont3d="3D/sity/Smart_City_Test_Cubes6.json"><div class="shine_red mini-but-s"></div></div>
	</div>
</div>


<script>
var idTestCanva = 'canvas3d_2t';
Load_Canvas_3D(idTestCanva,'3D/asu/Diag_ASU3_color.json');
</script>

</div>
</div>
</div>
<script src="js/table-tauch.js"></script>
</body>
</html>