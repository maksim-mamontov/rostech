$(function() {

create_page(windowCreate); // создаем окна и кнопки на странице

// $('#info2').html('x = '+x+'<br> y = '+y);

if (rotaryRevers){
	var buff = rotaryHoriz;
	rotaryHoriz = rotaryNone;
	rotaryNone = buff;
}		

	// touch block изменение размеров блока
	$(windo).on('touchstart',blockResize,function(event) { // на все блоки blockTarget повешаем эвент (даже если их нету)
		var timeStart = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var sElx = blockElem.position().left; // расположение ЛЕФТ элемента
		var sEly = blockElem.position().top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(),contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}
		
		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'touch'
		};
		
		if (tt == 2) {
			var touchSA = event.targetTouches[0];
			var touchSB = event.targetTouches[1];
			data['sx'] = Math.round(Math.abs(touchSA.pageX - touchSB.pageX));
			data['sy'] = Math.round(Math.abs(touchSA.pageY - touchSB.pageY));
			
			resize_block(data);
		}
	});
	
	// движение блока
	$(windo).on('touchstart',blockTarget,function(event) { // на все блоки blockTarget повешаем эвент (даже если их нету)
		var timeStart = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var sElx = blockElem.position().left; // расположение ЛЕФТ элемента
		var sEly = blockElem.position().top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(),contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}

		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'touch'
		};
		
		if (tt == 1) {
			if (control2C != 1) {//двойной клик
				var GlobObject = gDataInfo['block_'+index];
				if (GlobObject){
					if (GlobObject.startTime+timerDB >= timeStart) {//двойной клик произашел
						/*
						var heightTE = $(elem).css('height');
						if (heightTE == 40+'px'){
							$(elem).animate({"height": 100+'%'},timerAnimBut);
							$(elem).children(targetOpen).html('&uarr;');
						}
						else {
							$(elem).animate({"height": 40+'px'},timerAnimBut);
							$(elem).children(targetOpen).html('&darr;');
						}
						*/
					}
					gDataInfo['block_'+index] = {'startTime':timeStart};
				}
				else {gDataInfo['block_'+index] = {'startTime':timeStart};}
			}	
			
			var touch = event.targetTouches[0];
			data['sx'] = touch.pageX;
			data['sy'] = touch.pageY;
			
			drag_block(data);
		}
	}),false;
	
	// движение иметирующее скрол, и свайп внутри блока 
	$(windo).on('touchstart',conInfoMove,function(event) { 
		var timeControl = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(conInfoWin); // получим окно в котором элемент
		var rotat = detect_rotate($(this).closest(rotaryBlock).css('transform'));// угол поворота текущего элемента
		// Element Window
		var elemH = elem.height(); // Высота элемента
		//var sEly = elem.position().top; // расположение  ТОП элемент
		var sEly = elem.css('top') // расположение  ТОП элемент
		sEly = Number(sEly.substr(0, sEly.length - 2)); // расположение  ТОП элемент
		var windH = blockElem.height(); // Высота окна
		// переменные для манипуляций
		var diff,y,ys,ye,ty,x,xs,tx,currentTime,lastY=sEly,swipX=false;
		diff = windH-elemH
		
		if (diff <= 0){
		$(elem).off('touchmove');
		var touch = event.targetTouches[0];
		ys = touch.pageY;
		xs = touch.pageX;
			if (rotat == 0){
				$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
					if (event.targetTouches.length == 1) { // 1 касание
						touchm = event.targetTouches[0];
						ty = touchm.pageY-ys;
						tx = touchm.pageX-xs;
						y = sEly+ty;
						if (y < diff) {y = diff;} if (y > 0) {y = 0;}
						if (tx > swipeX3d || tx < -swipeX3d){swipX = true;}
						
						currentTime = new Date().getTime();
						if (timeControl+timerKin <= currentTime) {
							timeControl = currentTime;
							lastY = y;
						}
						
						$(elem).css({'top':y+'px'});
					}
				}),false;					
			}
			if (rotat == 180){
				$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
					if (event.targetTouches.length == 1) { // 1 касание
						touchm = event.targetTouches[0];
						ty = ys-touchm.pageY;
						tx = touchm.pageX-xs;
						y = sEly+ty;
						if (y < diff) {y = diff;} if (y > 0) {y = 0;}
						if (tx > swipeX3d || tx < -swipeX3d){swipX = true;}
						
						currentTime = new Date().getTime();
						if (timeControl+timerKin <= currentTime) {
							timeControl = currentTime;
							lastY = y;
						}
						
						$(elem).css({'top':y+'px'});
						}
				}),false;
			}
			
			if (kinetic){
				$(elem).off('touchend');$(elem).off('mouseup');
				$(elem).one('touchend mouseup',function(event) {
					ye = (y-lastY)*kofDis;
					if (swipX){back_3D(elem);}
					animate({
						duration:timerAnim,
						timing: makeEaseOut(circ),
						draw: function(prog){
							ym = y+(prog*ye);
							if (winFrame){
								if (ym < diff) {ym = diff;} if (ym > 0) {ym = 0;}
							}
							$(elem).css({'top':ym});
						}
					});
				});
			}

		}
	});
	
	
	// mouse
	$(windo).on('mousedown',blockTarget,function(event) {
		var timeStart = new Date().getTime(); // время клика на объект
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var elXY = blockElem.position();
		var sElx = elXY.left; // расположение ЛЕФТ элемента
		var sEly = elXY.top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(), contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}

		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'mouse'
		};
		
		if (control2C != 1) {//двойной клик
			var GlobObject = gDataInfo['block_'+index];
			if (GlobObject){
				if (GlobObject.startTime+timerDB >= timeStart) {// если 2 клик произашел
					/*
					var heightTE = $(elem).css('height');
					if (heightTE == 40+'px'){
						$(elem).animate({"height": 100+'%'},timerAnimBut);
						$(elem).children(targetOpen).html('&uarr;');
					}
					else {
						$(elem).animate({"height": 40+'px'},timerAnimBut);
						$(elem).children(targetOpen).html('&darr;');
					}
					*/
				}
				gDataInfo['block_'+index] = {'startTime':timeStart};
			}
			else {gDataInfo['block_'+index] = {'startTime':timeStart};}
		}	
		
		data['sx'] = event.clientX;
		data['sy'] = event.clientY;

		drag_block(data);
	});
	
	
	// Delete block
	// функция удаления блоков со страници, если по классу blockDelete был клик то удалит по умолчанию весь block со страници
	// но если у класса blockDelete есть data-delet="класс удоляемого объекта" удалит объект с классом из data-delet, если он родитель или выше 
	$(windo).on('click touchstart',blockDelete,function(event) {
		var dataDel = $(this).data();
		
		var elemDel = $(this).closest(block);
		if (dataDel.delet) {elemDel = $(this).closest('.'+dataDel.delet);}
		var elemId3d = $(elemDel).find(canvas3d).attr("id");
		
		if (elemId3d) { // если ИД 3д существует, сначала выгружаем 3д 
			b4w.require(elemId3d,elemId3d).unload_cb();
		}
		elemDel.remove(); // удоляем элемент со страници
	});
	
	// clouse dop info in 3D. закрывает дополнительный контент внутри 3д
	$(windo).on('click touchstart',back3d,function(){
		back_3D(this);
	});
	
	
	// Open Control Menu 
	$(windo).on('click touchstart',targetOpen,function(event) {
		var glBlock = $(this).closest(block); // получим основной блок
		var resizeBlock = $(glBlock).find(blockOpen); // блок который открываем
		var heightReBlock = $(resizeBlock)[0].offsetHeight; // высота блока
		var heigWiReBlock = $(resizeBlock)[0].offsetWidth; // ширина блока
		if (typeOpen == "top") {
			if (heightReBlock > 0){
				animate({
					duration:timerAnimBut,
					draw: function(prog){resizeBlock[0].style.height = 100*(1-prog)+'%';}
				});
				$(this).html(arreyOpen);
			}
			else {
				animate({
					duration:timerAnimBut,
					draw: function(prog){resizeBlock[0].style.height = 100*prog+'%';}
				});
				$(this).html(arreyClos);
			}
		}
		if (typeOpen == "right") {
			if (heigWiReBlock > 0){
				animate({
					duration:timerAnimBut,
					draw: function(prog){resizeBlock[0].style.width = 100*(1-prog)+'%';}
				});
				$(this).html(arreyOpen);
				$(this).css({'color':'#fff'});
			}
			else {
				animate({
					duration:timerAnimBut,
					draw: function(prog){resizeBlock[0].style.width = 100*prog+'%';}
				});
				$(this).html(arreyClos);
				$(this).css({'color':'#f00'});
			}
		}

	});
	
	
	// mini button click
	$(butСС).on('click touchstart', function(event) {
		var winCont = $(this).closest(windowControl)
		var topWin =  $(winCont).position().top;
		var lefWin = $(winCont).position().left;
		var cloneInfo = $(this).find(cloneContent);
		var dataInfo = cloneInfo.data();
		var elemTop=0,elemLeft=0,specClass='block-object',pastContent,cloneElem,RBC,border,background,idNamme,createBlock,styleOpen;

		elemTop = (dataInfo.top) ? (dataInfo.top+topWin):0;
		elemLeft = (dataInfo.left) ? (dataInfo.left+lefWin):0;

		
		if (dataInfo.type) {
			if (dataInfo.rotary) {RBC = dataInfo.rotary;}
			else {RBC = detect_rotate($(this).closest(windowControl).css('transform'));}
			if (dataInfo.type == 'content') {
				createBlock = true;
				pastContent = $(this).find(cloneInfo).html();
			}
			else if (dataInfo.type == 'img') {
				if (dataInfo.src) {
					specClass = 'block-img';
					pastContent = ''+
						'<div class="'+block.substr(1)+' '+specClass+'" style="top:'+elemTop+'px; left:'+elemLeft+'px;" data-control2c="1" data-content="img">'+
							'<div class="'+rotaryBlock.substr(1)+'" style="transform: rotate('+RBC+'deg);">'+
								'<div class="'+blockTarget.substr(1)+'">'+
									'<img src="'+dataInfo.src+'">'+
									'<div class="'+blockDelete.substr(1)+'">×</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'';
				}
			}
			else if (dataInfo.type == 'iframe') {
				if (dataInfo.src) {
					createBlock = true;
					pastContent = '<iframe src="'+dataInfo.src+'"></iframe>';
				}
			}
			else if (dataInfo.type == 'video'){
				createBlock = true;
				pastContent = ' <video controls="controls">\
									<source src="'+dataInfo.src+'">\
								</video>'
			}
			else if (dataInfo.type == '3d') {
				createBlock = true;
				id3D++;
				idNamme = 'canvas3d_'+id3D;
				pastContent = '<div class="'+canvas3d.substr(1)+'" id="'+idNamme+'"></div>';
				
			}
		}
		
		if (dataInfo) {
			if (dataInfo.border) {border = 'border:'+dataInfo.border+';';} else {border="";}
			if (dataInfo.back) { background = 'background:'+dataInfo.back+';';}else {background="";}
		}
		
		if (createBlock == true) {
			if (typeOpen == 'top') {styleOpen = 'style="top:0px; left:0px; width:100%; height:0px;"';}
			if (typeOpen == 'right') {styleOpen = 'style="top:0px; right:0px; width:0%; height:100%;"';}
			cloneElem = '<div class="'+block.substr(1)+' '+specClass+'" style="top:'+elemTop+'px; left:'+elemLeft+'px; '+border+' '+background+'">'+
			'<div class="'+rotaryBlock.substr(1)+'" style="transform: rotate('+RBC+'deg);">'+
				'<div class="'+blockTarget.substr(1)+'"></div>'+
				'<div class="'+targetOpen.substr(1)+'">'+arreyOpen+'</div>'+
				'<div class="'+blockDelete.substr(1)+'">×</div>'+
				'<div class="'+blockResize.substr(1)+' '+blockOpen.substr(1)+' '+blockTarget.substr(1)+'" '+styleOpen+'></div>'+
				'<div class="'+blokContent.substr(1)+'">'+
			pastContent+'</div></div></div>';
			$(windo).append(cloneElem);
		
			if (dataInfo.type == '3d' && dataInfo.src) {Load_Canvas_3D(idNamme,dataInfo.src);}
		}
		else {$(windo).append(pastContent);}
	});
	
	
	// background buttun
	$(butBС).on('click touchstart', function(event) {
		var cloneInfo = $(this).find(cloneContent);
		var dataInfo = cloneInfo.data();
		var target = $(this).closest(windowControl)
		
		if (dataInfo.type) {
		var bacSrc = dataInfo.src;
			if (dataInfo.type == 'background') {
				var background = $(target).css('background-image');
				if ( $(target).css('background-image') != 'none' ){
					$(target).css({'background-image':'none'});
				}
				else {
					$(target).css({'background-image':'url('+bacSrc+')'});
				}
			}
		}
	});
	
	
	// central button  cenButCli centralBut
	$(cenButCli).on('click touchstart', function(event) {
		var elem = $(this).closest(centralBut);
		remove_button(elem);
	});
	
	if (returnButton) {ad_window();} // функция анализирующая активно окно или нет
	
});

