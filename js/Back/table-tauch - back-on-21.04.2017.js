$(function() {

// $('#info2').html('x = '+x+'<br> y = '+y);

var winFrame = false // если правда блок небудет выходить за рамки, если лож будет
var kinetic = false // Физика движений для блоков, включить выключить.
	
var doc = document;
var block = '.block'; // класс или ид элемента который двигается
var blockTypeObj = '.block-content-object'; // тип объекта - объект
var blockTypeImg = '.block-content-img'; // тип объекта - картинка
var blockTarget = '.block-target'; // класс или ид элемента по которому двигаем
var blockCotrolMenu = '.open-control-menu'; // расскрывающемся меню в блоке
var windo = '.window'; // клас или ид элемента окна, в котором движущиеся блоки

var timerDB = 200 // таймер между кликами (для дабл клика)
var timerKin = 200; // таймер для инерции
var minElemW = 100 // минимальная ширина элемента
var minElemH = 100 // минимальная высота элемента
var maxElemW = 900 // максимальная высота элемента
var maxElemH = 900 // максимальная высота элемента

// отключить браузерные функции перетаскивания	
doc.ondragstart = function() {
  return false;
};

// отменяем тачь действия браузера по умолчанию.
$('.content').on('touchstart touchmove touchend', function(event) {
	event.preventDefault(); // Отменяет событие, если оно отменяемое
	event.stopPropagation(); // Прекращает дальнейшую передачу текущего события.
});
	
	//function 
	
	function drag_block (data){
		var elem = data['elem'];
		var sElx = data['sElx'];
		var sEly = data['sEly'];
		var sx = data['sx'];
		var sy = data['sy'];
		var tx = data['tx'];
		var ty = data['ty'];
		var xMax = data['xMax'];
		var yMax = data['yMax'];
		
		var x = Math.round((tx-sx)+sElx);
		var y = Math.round((ty-sy)+sEly);
		
		if (winFrame) { 
			if (x >= xMax) {x = xMax;} if (x <= 0) {x = 0;}
			if (y >= yMax) {y = yMax;} if (y <= 0) {y = 0;}
		}
		
		$(elem).css({'left' : x+'px','top':y+'px'});
	}
	function kinetick_move(data){
		var elem = data['elem'];
		var timeControl = data['timeControl']; // таймер контроля
		var lastX = data['lastX']; // последний Х (мышки)
		var lastY = data['lastY'];
		var sElx = data['sElx']; // положение Х (элемента) в момент нажатия
		var sEly = data['sEly'];
		var sx = data['sx']; // положение Х (мышки) в момент нажатия
		var sy = data['sy'];
		
		var elXY = elem.position();
		var tElx = elXY.left; // расположение ЛЕФТ элемента
		var tEly = elXY.top; // расположение  ТОП элемент
		
		var xdif = sx-sElx;
		lastX = lastX-xdif;
		var x = tElx-lastX;
		
		var ydif = sy-sEly;
		lastY = lastY-ydif;
		var y = tEly-lastY;
		
		
		var currentTime = new Date().getTime();
		if (timeControl+timerKin >= currentTime ) {
			var xOp = ( x > 0 ) ? "+=" + x : "-=" + Math.abs(x);
            var yOp = ( y > 0 ) ? "+=" + y : "-=" + Math.abs(y);
			
			$(elem).animate({"left": xOp+'px',"top": yOp+'px'}, "slow");
		}
		else {}
	}
	
	function resize_block(data){
		var elem = data['elem'];
		var x = data['ew']+data['elW'];
		var y = data['eh']+data['elH'];
		var lx = data['sElx']-(data['ew']/2);
		var ly = data['sEly']-(data['eh']/2);
		
		if (lx <= 0){lx = 0;} if (ly <= 0){ly = 0;}
		var xMax = maxElemW;
		var yMax = maxElemH;
		
		if (winFrame) { 
			if (x >= xMax){x = xMax;} if (y >= yMax){y = yMax;}
			if (x <= minElemW) {x = minElemW;} if (y <= minElemH) {y = minElemH;}
		}
		
		$(elem).css({'height':y,'width':x});
		$(elem).css({'left' : lx+'px','top':ly+'px'});
	}

	function DBclik (elem,lastTap,timeout){
		var currentTime = new Date().getTime();
		var tapLength = currentTime - lastTap;   
		clearTimeout(timeout);

		if(tapLength < timerDB && tapLength > 0){
			var control = $(elem).hasClass("open-control-menu");
			if ( control ) {$(elem).removeClass("open-control-menu");}
			else {$(elem).addClass("open-control-menu");}
		}else{
			timeout = setTimeout(function(){clearTimeout(timeout); }, timerDB);
		}
		lastTap = currentTime;

		var data = {'lastTap':lastTap,'timeout':timeout};
		return data;		
	}
	
	// double touch
	var timeout;
	var lastTap = 0;
	// double touch END
	
	$(blockTarget).on('touchstart mousedown', function(event) {
		var elem = $(this); // получи нажатый элемент
		var elemetn = $(this).closest(block); // получим основной движемый элемент	
		var timeControl = 0;
		var elemetnControl = false;
		var objecType = false;

		if ($(this).closest(blockTypeImg).length != 0) {objecType = 'img';}
		else if ($(this).closest(blockTypeImg).length != 0){objecType = 'obj';}
		
		if  ( $(this).hasClass("open-control-menu")) {elemetnControl = true;}
		else {elemetnControl = false;}
		if (objecType == 'img') {elemetnControl == true}
		
		if (elemetn.length != 0) {
			var sx,sy; // глобалки для отслеживания кординат при нажатии
			var elW = elemetn.width(); // ширина элемента
			var elH = elemetn.height(); // Высота элемента
			
			var elXY = elemetn.position();
			var sElx = elXY.left; // расположение ЛЕФТ элемента
			var sEly = elXY.top; // расположение  ТОП элемент
			
			var panel = $(elemetn).closest(windo); // получим главное окно
			var Ww = panel.width(); // ширина рамки главного окна
			var Wh = panel.height(); // Высота рамки главного окна
			var xMax = Ww-elW; // ограницение за выход рамки по Х
			var yMax = Wh-elH; // ограницение за выход рамки по У

			var data = {};
			data = {
				'elW':elW,
				'elH':elH,
				'sElx':sElx,
				'sEly':sEly,
				'elem':elemetn,
				'Ww':Ww,
				'Wh':Wh,
				'yMax':yMax,
				'xMax':xMax,
			};
			// работа с тачем
			if (event.type == 'touchstart') { // если нажали пальцем
				var mouseMove = 1;
				timeControl = new Date().getTime();
				var tt = event.targetTouches;
				if (tt.length == 1){ // если нажали 1м пальцем
					var timeData = DBclik(elem,lastTap,timeout);
					timeout = timeData['timeout'];
					lastTap = timeData['lastTap'];
						
					elemetn.addClass('active-mov');
					var touch = event.targetTouches[0];
					data['sx'] = touch.pageX;
					data['sy'] = touch.pageY;
					
					
					$(this).on('touchmove',function(event) { // если тянем 1 пальцем
						if (event.targetTouches.length == 1) { // 1 касание		
							var touchm = event.targetTouches[0];
							data['tx'] = touchm.pageX;
							data['ty'] = touchm.pageY;
							
							var currentTime = new Date().getTime();
							if (timeControl+timerKin <= currentTime) {
								mouseMove = 2;
								timeControl = currentTime;
								data['timeControl'] = currentTime;
								data['lastX'] = touchm.pageX;
								data['lastY'] = touchm.pageY;
							}
							
							drag_block(data);
						}
					}),false;
					
				
					$(elemetn).on('touchend',function(event) { // если отпустили пальцем
						elemetn.removeClass('active-mov');
						if (mouseMove == 2){
							//kinetick_move(data);
						}
					}),false;
				}

				if (tt.length == 2 &&  elemetnControl == true){
					var touchSA = event.targetTouches[0];
					var touchSB = event.targetTouches[1];
					var sx = Math.abs(touchSA.pageX - touchSB.pageX);
					var sy = Math.abs(touchSA.pageY - touchSB.pageY);
					$(this).on('touchmove',function(event) { // если тянем 2мя пальцами
						if (event.targetTouches.length == 2 &&  elemetnControl == true) {
							var touchA = event.targetTouches[0];
							var touchB = event.targetTouches[1];
							data['ew'] = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
							data['eh'] = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
							resize_block(data);
						}
					}),false;
				}
			}
			
			// работа с мышю
			if (event.type == 'mousedown') { // если кликнули мышю
			var mouseMove = 1;
			timeControl = new Date().getTime();
			var timeData = DBclik(elem,lastTap,timeout);
				timeout = timeData['timeout'];
				lastTap = timeData['lastTap'];
				elemetn.addClass('active-mov');
				data['sx'] = event.clientX;
				data['sy'] = event.clientY;
				
				doc.onmousemove = function(event) { // если водим мышью
					data['tx'] = event.clientX;
					data['ty'] = event.clientY

					var currentTime = new Date().getTime();
					if (timeControl+timerKin <= currentTime) {
						mouseMove = 2;
						timeControl = currentTime;
						data['timeControl'] = currentTime;
						data['lastX'] = event.clientX;
						data['lastY'] = event.clientY;
					}
					drag_block(data);
				}
				
				doc.onmouseup = function(event) { // если отпустили мыш
					doc.onmousemove = null;
					$(elemetn).onmousemove = null;
					elemetn.removeClass('active-mov');
					if (mouseMove == 2){
						kinetick_move(data);
					}
				}
			}
		}
	}),false;
	
});

