$(function() {

// $('#info2').html('x = '+x+'<br> y = '+y);

var winFrame = true; // если правда блок небудет выходить за рамки, своего родителя, если лож будет
var kinetic = true; // Физика движений для блоков, включить выключить.
var rotaryL = true; // включение переворачивания блоков после линии
var rotaryRevers = true; // включить обратное отображение блоков ( зеркально реверсное )
	
var doc = document;
var block = '.block'; // класс или ид элемента который двигается
var blockImg = '.block-img'; // класс или ид элемента который двигается
var blockTarget = '.block-target'; // класс или ид элемента с помощью которого двигаем
var blockDelete = '.block-delete'; // класс при клики на который удоляем блок
var targetOpen = '.open-control-menu'; // класс при клике на который раскрывается таргет меню
var butСС = '.but-create-content'; // кнопка создания новыхх блоков 
var butBС = '.but-background-content'; // кнопка изменения фонового изображения
var cloneContent = '.clone-content'; // все что внутри этого блока клонировать на главный экран
var windo = '.content'; // клас или ид элемента окна, в котором движущиеся блоки

//центральная кнопка
var centralBut = '.central-but'; // центральная кнопка на дисплее
var cenButCli = '.central-but-click' // блок на который нажимаем для работы центральной кнопки
	var dropDown = -60; // при клике опускать на это виличену
	var dropUp = 80; // при клике поднимать на это виличену
	var ContTimAnim = 400; // время анимаций контрольной кнопки

		
var targetCotrolH = 40; // высота окна таргета (должна быть как и в css) 

// rotary line block
var rotaryBlock = '.block-rotary'; // блок внутри которого все перевернется
var rotaryLine = 539; // высота отнасительно TOP после которой блок перевернется
var rotaryNone = '0'; // поворот блока при создании
var rotaryHoriz = 180; // горинзонтальный поворот

var timerDB = 200; // таймер между кликами (для дабл клика)
var timerKin = 200; // таймер для инерции (время в которое фиксируется растояние пройденное пальцем) 
var timerAnim = 400; // таймер Анимации физики элемента (время которое летит элемент после броска)
var timerAnimBut = 200; // таймер Анимации Кнопок (время раскрывания закрывания кнопок)

var kofDis = 2; // коофицент дистанции при кинематике ( растояние которое пролетит блок после отпускания )
var minElemW = 180; // минимальная ширина элемента
var minElemH = 180; // минимальная высота элемента
var maxElemW = 900; // максимальная высота элемента
var maxElemH = 900; // максимальная высота элемента


var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
var gDataInfo = {}; // Объект с глобальными переменными по блокам
var id3D = 1; // глобальная переменная для отслеживания ИД 3д объекта

//function 

if (rotaryRevers){
	var buff = rotaryHoriz;
	rotaryHoriz = rotaryNone;
	rotaryNone = buff;
}
				
	// Animation 
	function animate(options) {
	  var start = performance.now();
	  requestAnimationFrame(function animate(time) {
		// timeFraction от 0 до 1
		var timeFraction = (time - start) / options.duration;
		if (timeFraction > 1) timeFraction = 1;
		// текущее состояние анимации
		var progress = options.timing(timeFraction)
		options.draw(progress);
		if (timeFraction < 1) {
		  requestAnimationFrame(animate);
		}
	  });
	}
		
	function circ(timeFraction) {
	  return 1 - Math.sin(Math.acos(timeFraction))
	}
	
	function makeEaseOut(timing) {
	  return function(timeFraction) {
		return 1 - timing(1 - timeFraction);
	  }
	}
		
	// Drag and Drob block
	function drag_block(data){
		var elem = data['elem']; // сам элемент
		var sElx = data['sElx']; // положение элемента при старте (топ)
		var sEly = data['sEly']; // положение элемента при старте (левт)
		var	elW = data['elW']; // ширина элемента
		var	elH = data['elH']; // высота элемента
		var sx = data['sx']; // положение пальца при клике (топ)
		var sy = data['sy']; // положение пальца при клике (лефт)
		var Ww = data['Ww']; // ширина главного окна
		var Wh = data['Wh']; // Высота главного окна
		var typeEvent = data['typeEvent']; // Высота главного окна
		var resizeControl = data['resizeControl'];
		var tx,ty,x,y,lastX=sx,lastY=sy,xMax,yMax,touchm;	
		var timeControl = new Date().getTime();
		var currentTime = new Date().getTime();
	
		var elemMove = data['blockElem']; // элемент который нужно двигать
		if (!elemMove){elemMove = elem;}
		
		function drag(){
			x = Math.round((tx-sx)+sElx);
			y = Math.round((ty-sy)+sEly);
			
			if (kinetic){
				currentTime = new Date().getTime();
				if (timeControl+timerKin <= currentTime) {
					mouseMove = 2;
					timeControl = currentTime;
					lastX = tx;
					lastY = ty;
				}
			}
			
			if (winFrame) {
				xMax = Math.round(Ww-elW);
				yMax = Math.round(Wh-elH);
				if (x >= xMax){x = xMax;} if (x <= 0) {x = 0;}
				if (y >= yMax){y = yMax;} if (y <= 0) {y = 0;}
			}
			
			$(elemMove).css({'left' : x+'px','top':y+'px'})
		}
		
		if (typeEvent == 'touch') {
			$(elem).off('touchmove');
			$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
				if (event.targetTouches.length == 1) { // 1 касание
					touchm = event.targetTouches[0];
					tx = touchm.pageX;
					ty = touchm.pageY;
					drag();
				}
			}),false;

		}
		if (typeEvent == 'mouse') { 
			$(elem).off('mousemove');
			doc.onmousemove = function(event) {
				tx = event.clientX;
				ty = event.clientY
				drag();
			}
		}
		
		$(elem).off('touchend');$(elem).off('mouseup');
		$(elem).one('touchend mouseup',function(event) {
			$(elem).off('touchmove');$(elem).off('mousemove');
			doc.onmousemove = null;
			$(elem).onmousemove = null;
			var yRotaryControl = y;
			if (kinetic){ // kinetic move
				var xdif,xk,ydif,yk,kofX,kofY,xm,ym;
				var timeUp = new Date().getTime();
				
				xdif = sx-sElx;
				lastX = lastX-xdif;
				xk = x-lastX;
				
				ydif = sy-sEly;
				lastY = lastY-ydif;
				yk = y-lastY;
				
				var distanceX = xk*kofDis;
				var distanceY = yk*kofDis;
				
				if (timeUp <= currentTime+200){
					animate({
						duration:timerAnim,
						timing: makeEaseOut(circ),
						draw: function(prog){
							xm = x+(prog*distanceX);
							ym = y+(prog*distanceY);
							if (winFrame){
								if (xm >= xMax) {xm = xMax;} if (xm <= 0) {xm = 0;}
								if (ym >= yMax) {ym = yMax;} if (ym <= 0) {ym = 0;}
							}
							$(elemMove).css({'left':xm,'top':ym});
						}
					});
				}
				yRotaryControl = y+distanceY;
			}
			if (rotaryL) {
				if (!yRotaryControl) {yRotaryControl = sEly;}
				if (yRotaryControl+(elH/2) >= rotaryLine) {
					if (resizeControl) {$(elem).find(rotaryBlock).css({'transform':'rotate('+rotaryHoriz+'deg)'});}
					else {$(elem).closest(rotaryBlock).css({'transform':'rotate('+rotaryHoriz+'deg)'});}
				}else {
					if (resizeControl) {$(elem).find(rotaryBlock).css({'transform':'rotate('+rotaryNone+'deg)'});}
					else {$(elem).closest(rotaryBlock).css({'transform':'rotate('+rotaryNone+'deg)'});}
				}
			}
		}),false;
	};
	
	// Resize Block
	function resize_block(data){
		var elem = data['elem'];
		var sx = data['sx'];
		var sy = data['sy'];
		var contentBlock = data['contentBlock'];
		var ew,eh,x,y,lx,ly;
		
		var elemMove = data['blockElem'];
		if (!elemMove){elemMove = elem;}
		
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 2мя пальцами
			if (event.targetTouches.length == 2) {
				var touchA = event.targetTouches[0];
				var touchB = event.targetTouches[1];
				ew = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
				eh = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
				
				if (contentBlock == 'img') {
					x = (eh+ew)+data['elW'];
				}
				else {y = eh+data['elH']; x = ew+data['elW'];}
				
				lx = data['sElx']-(ew/2);
				ly = data['sEly']-(eh/2);
								
				if (x <= minElemW) {x = minElemW;} if (x >= maxElemW) {x = maxElemW;}
				if (y <= minElemH) {y = minElemH;} if (y >= maxElemH) {y = maxElemH;}

				$(elemMove).css({'height':y,'width':x});
				$(elemMove).css({'left' : lx+'px','top':ly+'px'});
			}
		}),false;
		
		$(elem).off('touchend');
		$(elem).one('touchend',function(event) {
			$(elem).off('touchmove');
			
		}),false;
	}
	
	// Rotary block
	function rotary_block(data){
		var elem = data['elem'];
		var sx = data['sx'];
		var sy = data['sy'];
		var ew,eh,x,y,rotate;
		var rotateO = 0;
		
		var elemMove = data['blockElem'];
		if (!elemMove){elemMove = elem;}
		
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 2мя пальцами
			if (event.targetTouches.length >= 3) {
				var touchA = event.targetTouches[0];
				var touchB = event.targetTouches[1];
				ew = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
				eh = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
				
				x = ew+data['elW'];
				y = eh+data['elH'];
				rotate = x-y;
				
				//elem.rotate(rotate);
				if (rotate >= 90) {rotateO = 0;}
				if (rotate <= 90) {rotateO = 180;}
				if (rotate >= -90) {rotateO = 0;}
				if (rotate <= -90) {rotateO = 180;}
				$('#info1').html('rotate = '+rotate+'<br> y = '+y)
				$(elemMove).css({'transform': 'rotate('+rotate+'deg)'});
				//$(elem).animate({'transform': 'rotate('+rotateO+'deg)'});
				//$(elem).css({'left' : lx+'px','top':ly+'px'});
			}
		}),false;
		
		$(elem).off('touchend');
		$(elem).one('touchend',function(event) {
			$(elem).off('touchmove');
			
			$(elemMove).css({'transform': 'rotate('+rotateO+'deg)'});
			
		}),false;
	}

	// touch block
	$(windo).on('touchstart',blockTarget,function(event) { // на все блоки blockTarget повешаем эвент (даже если их нету)
		var timeStart = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var elXY = blockElem.position();
		var sElx = elXY.left; // расположение ЛЕФТ элемента
		var sEly = elXY.top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(),contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}
		
		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'touch'
		};
		
		if (tt == 1) {
			if (control2C != 1) {//двойной клик
				var GlobObject = gDataInfo['block_'+index];
				if (GlobObject){
					if (GlobObject.startTime+timerDB >= timeStart) {
						var heightTE = $(elem).css('height');
						if (heightTE == targetCotrolH+'px'){
							$(elem).animate({"height": 100+'%'},timerAnimBut);
							$(elem).children(targetOpen).html('&uarr;');
						}
						else {
							$(elem).animate({"height": targetCotrolH+'px'},timerAnimBut);
							$(elem).children(targetOpen).html('&darr;');
						}
					}
					gDataInfo['block_'+index] = {'startTime':timeStart};
				}
				else {gDataInfo['block_'+index] = {'startTime':timeStart};}
			}	
			
			var touch = event.targetTouches[0];
			data['sx'] = touch.pageX;
			data['sy'] = touch.pageY;
			
			drag_block(data);
		}
		
		else if (tt == 2) {
			var touchSA = event.targetTouches[0];
			var touchSB = event.targetTouches[1];
			data['sx'] = Math.round(Math.abs(touchSA.pageX - touchSB.pageX));
			data['sy'] = Math.round(Math.abs(touchSA.pageY - touchSB.pageY));
			
			resize_block(data);
		}

		else if (tt >= 3) {	
			var touchSA = event.targetTouches[0];
			var touchSB = event.targetTouches[1];
			data['sx'] = Math.round(Math.abs(touchSA.pageX - touchSB.pageX));
			data['sy'] = Math.round(Math.abs(touchSA.pageY - touchSB.pageY));
			
			//rotary_block(data);
		}
	}),false;
	
	// mouse
	$(windo).on('mousedown',blockTarget,function(event) {
		var timeStart = new Date().getTime(); // время клика на объект
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var elXY = blockElem.position();
		var sElx = elXY.left; // расположение ЛЕФТ элемента
		var sEly = elXY.top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(),contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}

		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'mouse'
		};
		
		if (control2C != 1) {//двойной клик
			var GlobObject = gDataInfo['block_'+index];
			if (GlobObject){
				if (GlobObject.startTime+timerDB >= timeStart) {
					var heightTE = $(elem).css('height');
					if (heightTE == targetCotrolH+'px'){
						$(elem).animate({"height": 100+'%'},timerAnimBut);
						$(elem).children(targetOpen).html('&uarr;');
					}
					else {
						$(elem).animate({"height": targetCotrolH+'px'},timerAnimBut);
						$(elem).children(targetOpen).html('&darr;');
					}
				}
				gDataInfo['block_'+index] = {'startTime':timeStart};
			}
			else {gDataInfo['block_'+index] = {'startTime':timeStart};}
		}	
		
		data['sx'] = event.clientX;
		data['sy'] = event.clientY;

		drag_block(data);
	});
	
	// Delete block
	$(windo).on('click touchstart',blockDelete,function(event) {
		$(this).closest(block).remove()
	});
	
	// Open Control Menu
	$(windo).on('click touchstart',targetOpen,function(event) {
		var elem = $(this).closest(blockTarget); // получим основной движемый элемент	
		var heightTE = $(elem).css('height');	
		if (heightTE == targetCotrolH+'px') {
			$(elem).animate({"height": 100+'%'},timerAnimBut);
			$(this).html('&uarr;');
		}
		else {
			$(elem).animate({"height": targetCotrolH+'px'},timerAnimBut);
			$(this).html('&darr;');
		}
	});
	
	// mini button click
	$(butСС).on('click touchstart', function(event) {
		var cloneInfo = $(this).find(cloneContent);
		var dataInfo = cloneInfo.data();
		var elemTop=0,elemLeft=0,specClass='block-object',pastContent,cloneElem,RBC,border,background,idNamme,createBlock;
		
		if (dataInfo.position) {
			elemTop = (dataInfo.position.top) ? dataInfo.position.top:0;
			elemLeft = (dataInfo.position.left) ? dataInfo.position.left:0;
		}
		
		if (dataInfo.object) {
			if (dataInfo.rotary) {RBC = dataInfo.rotary;}
			else {RBC = '0';}
			if (dataInfo.object == 'content') {
				createBlock = true;
				pastContent = $(this).find(cloneInfo).html();
			}
			else if (dataInfo.object == 'img') {
				if (dataInfo.src) {
					specClass = 'block-img';
					pastContent = '\
						<div class="block '+specClass+'" style="top:'+elemTop+'px; left:'+elemLeft+'px;" data-control2c="1" data-content="img">\
							<div class="block-rotary" style="transform: rotate('+RBC+'deg);">\
								<div class="block-target">\
									<img src="'+dataInfo.src+'">\
									<div class="block-delete">×</div>\
								</div>\
							</div>\
						</div>\
					';
				}
			}
			else if (dataInfo.object == 'iframe') {
				if (dataInfo.src) {
					createBlock = true;
					pastContent = '<iframe src="'+dataInfo.src+'"></iframe>';
				}
			}
			else if (dataInfo.object == 'video'){
				createBlock = true;
				pastContent = ' <video controls="controls">\
									<source src="'+dataInfo.src+'">\
								</video>'
			}
			else if (dataInfo.object == '3d') {
				createBlock = true;
				id3D++;
				idNamme = 'canvas3d_'+id3D;
				pastContent = '<div class="canvas3d" id="'+idNamme+'"></div>';
				
			}
		}
		
		if (dataInfo) {
			if (dataInfo.border) {border = dataInfo.border;}
			if (dataInfo.back) { background = dataInfo.back;}
		}
		
		if (createBlock == true) {
			cloneElem = '<div class="block '+specClass+'" style="top:'+elemTop+'px; left:'+elemLeft+'px; border:'+border+';background:'+background+'">\
			<div class="block-rotary" style="transform: rotate('+RBC+'deg);">\
				<div class="block-target">\
					<div class="open-control-menu">&darr;</div>\
					<div class="block-delete">&times;</div>\
				</div>\
				<div class="blok-content">\
			'+pastContent+' <div id="sm_city" style="position:absolute; top: 0px;">sm_city</div> </div></div></div>';
			$(windo).append(cloneElem);
		
			if (dataInfo.object == '3d' && dataInfo.src) {Load_Canvas_3D(idNamme,dataInfo.src);}
		}
		else {$(windo).append(pastContent);}
	});
	
	
	// background buttun
	$(butBС).on('click touchstart', function(event) {
		var cloneInfo = $(this).find(cloneContent);
		var dataInfo = cloneInfo.data();
		
		if (dataInfo.object) {
		var target = dataInfo.targetid;
		var bacSrc = dataInfo.src;
			if (dataInfo.object == 'background') {
				var background = $('#'+target).css('background-image');
				if ( $('#'+target).css('background-image') != 'none' ){
					$('#'+target).css({'background-image':'none'});
				}
				else {
					$('#'+target).css({'background-image':'url('+bacSrc+')'});
				}
			}
		}
	});
	
	
	// central button  cenButCli centralBut
	$(cenButCli).on('click touchstart', function(event) {
		var elem = $(this).closest(centralBut);
		var posBot = $(elem).css('bottom');
		
		if (posBot == dropUp+'px') {  //опускаем
			$(elem).animate({"bottom": dropDown+'px'},ContTimAnim,"linear");
			$(elem).find('.centr-mini-but').animate({'opacity': '1.0'},ContTimAnim,"linear");
			$(elem).find('.centr-mini-but').css({'display':'block'});
			console.log();
		}
		else { //поднимаем
			$(elem).animate({"bottom": dropUp+'px'},ContTimAnim,"linear");
			$(elem).find('.centr-mini-but').animate({'opacity': '0.0'},ContTimAnim,"linear",function(){
				$(elem).find('.centr-mini-but').css({'display':'none','opacity': '0.0'});
			});
		}
	});
});

