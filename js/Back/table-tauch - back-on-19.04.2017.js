$(function() {
	
var doc = document;
var block = '.block'; // класс или ид элемента который двигается
var windo = '.window'; // клас или ид элемента окна, в котором движущиеся блоки
var target = 'target-w'; // клас или ид целевая панель ( котороя направит блок который в ней в другую целевую панель )
var touchSumElem = 0; // количество нажатых пальцев на объекте

// отключить браузерные функции перетаскивания	
doc.ondragstart = function() {
  return false;
};

// отменяем тачь действия браузера по умолчанию.
$('.content').on('touchstart touchmove touchend', function(event) {
	event.preventDefault(); // Отменяет событие, если оно отменяемое
	event.stopPropagation(); // Прекращает дальнейшую передачу текущего события.
});
	
	//function 
	
	function drag_block (data){
		var elem = data['elem'];
		var sElx = data['sElx'];
		var sEly = data['sEly'];
		var sx = data['sx'];
		var sy = data['sy'];
		var tx = data['tx'];
		var ty = data['ty'];
		var xMax = data['xMax'];
		var yMax = data['yMax'];
		
		var x = (tx-sx)+sElx;
		var y = (ty-sy)+sEly;
		
		if (x >= xMax) {x = xMax;} if (x <= 0) {x = 0;}
		if (y >= yMax) {y = yMax;} if (y <= 0) {y = 0;}
		$(elem).css({'left' : x,'top':y});
		//$('#info').html('xMax= ' + xMax + '<br> yMax = ' + yMax);
		//$('#info1').html('x= ' + x + '<br> y = ' + y);
	}
	
	$(block).on('touchstart',function(event) {
		var tt = event.targetTouches;
		if (tt.length == 1) {touchSumElem = 1;}
		if (tt.length == 2) {touchSumElem = 2;}
	}),false;
	
	$(block).on('touchstart mousedown', function(event) {
		var elemetn = $(this); // получим основной движемый элемент
		var sx,sy; // глобалки для отслеживания кординат при нажатии
		var elW = elemetn.width(); // ширина элемента
		var elH = elemetn.height(); // Высота элемента
		
		var elXY = elemetn.position();
		var sElx = elXY.left;
		var sEly = elXY.top;
		
		var panel = $(elemetn).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var xMax = Ww-elW; // ограницение за выход рамки по Х
		var yMax = Wh-elH; // ограницение за выход рамки по У
		
		var data = {
			'sElx':sElx,
			'sEly':sEly,
			'elem':elemetn,
			'xMax':xMax,
			'yMax':yMax
		};
		
		var dataEnd = {
			'panel':panel
		}

		// работа с тачем
		if (event.type == 'touchstart') { // если нажали пальцем
			var tt = event.targetTouches;
			if (tt.length == 1){
				elemetn.addClass('active-mov');
				var touch = event.targetTouches[0];
				data['sx'] = touch.pageX;
				data['sy'] = touch.pageY;
				
				$(this).on('touchmove',function(event) { // если тянем пальцем
					if (event.targetTouches.length == 1) { // 1 касание
						var touchm = event.targetTouches[0];
						data['tx'] = touchm.pageX;
						data['ty'] = touchm.pageY;
						drag_block(data);
					}
				});
				
				$(this).on('touchend',function(event) { // если отпустили пальцем
					elemetn.removeClass('active-mov');
				});
				$('#info1').html('1 touch');
			}
		}
		

		
		// работа с мышю
		if (event.type == 'mousedown') { // если кликнули мышю
			elemetn.addClass('active-mov');
			data['sx'] = event.clientX;
			data['sy'] = event.clientY;
			
			doc.onmousemove = function(event) { // если водим мышью
				data['tx'] = event.clientX;
				data['ty'] = event.clientY
				drag_block(data);
			}
			
			doc.onmouseup = function(event) { // если отпустили мыш
				doc.onmousemove = null;
				$(elemetn).onmousemove = null;
				elemetn.removeClass('active-mov');
			}
		}
		
	});
	
	
	/*
	////*Определяем некоторые переменные
	var objzoom = document.getElementById("dro");
	var scaling = false;
	var dist = 0;
	var scale_factor = 1.0;
	var curr_scale = 1.0;
	var max_zoom = 8.0;
	var min_zoom = 0.5
	////*Пишем функцию, которая определяет расстояние меж пальцами
	function distance (p1, p2) {
	return (Math.sqrt(Math.pow((p1.clientX - p2.clientX), 2) + Math.pow((p1.clientY - p2.clientY), 2)));
	}
	
	////*Ловим начало косания
	objzoom.addEventListener('touchstart', function(evt) {
	evt.preventDefault();
	var tt = evt.targetTouches;
	if (tt.length >= 2) {
	dist = distance(tt[0], tt[1]);
	scaling = true;
	} else {
	scaling = false;
	}
	}, false);
	/////*Ловим зумирование
	objzoom.addEventListener('touchmove', function(evt) {
	evt.preventDefault();
	var tt = evt.targetTouches;
	if (scaling) {
	curr_scale = distance(tt[0], tt[1]) / dist * scale_factor;
	objzoom.style.WebkitTransform = "scale(" + curr_scale + ", " + curr_scale + ")";
	}
	}, false);
	///Ловим конец косания
	objzoom.addEventListener('touchend', function(evt) {
	var tt = evt.targetTouches;
	if (tt.length < 2) {
	scaling = false;
	if (curr_scale < min_zoom) {
	scale_factor = min_zoom;
	} else {
	if (curr_scale > max_zoom) {
	scale_factor = max_zoom;
	} else {
	scale_factor = curr_scale;
	}
	}
	objzoom.style.WebkitTransform = "scale(" + scale_factor + ", " + scale_factor + ")";
	} else {
	scaling = true;
	}
	}, false);
	*/
	
	
});

