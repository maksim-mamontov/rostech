$(function() {

// $('#info2').html('x = '+x+'<br> y = '+y);

if (rotaryRevers){
	var buff = rotaryHoriz;
	rotaryHoriz = rotaryNone;
	rotaryNone = buff;
}
				
	// Animation 
	function animate(options) {
	  var start = performance.now();
	  requestAnimationFrame(function animate(time) {
		// timeFraction от 0 до 1
		var timeFraction = (time - start) / options.duration;
		if (timeFraction > 1) timeFraction = 1;
		// текущее состояние анимации
		var progress = options.timing(timeFraction)
		options.draw(progress);
		if (timeFraction < 1) {
		  requestAnimationFrame(animate);
		}
	  });
	}
		
	function circ(timeFraction) {
	  return 1 - Math.sin(Math.acos(timeFraction))
	}
	
	function makeEaseOut(timing) {
	  return function(timeFraction) {
		return 1 - timing(1 - timeFraction);
	  }
	}
		
	// Drag and Drob block
	function drag_block(data){
		var elem = data['elem']; // сам элемент
		var sElx = data['sElx']; // положение элемента при старте (топ)
		var sEly = data['sEly']; // положение элемента при старте (левт)
		var	elW = data['elW']; // ширина элемента
		var	elH = data['elH']; // высота элемента
		var sx = data['sx']; // положение пальца при клике (топ)
		var sy = data['sy']; // положение пальца при клике (лефт)
		var Ww = data['Ww']; // ширина главного окна
		var Wh = data['Wh']; // Высота главного окна
		var typeEvent = data['typeEvent']; // Высота главного окна
		var resizeControl = data['resizeControl'];
		var tx,ty,x,y,lastX=sx,lastY=sy,xMax,yMax,touchm;	
		var timeControl = new Date().getTime();
		var currentTime = new Date().getTime();
	
		var elemMove = data['blockElem']; // элемент который нужно двигать
		if (!elemMove){elemMove = elem;}
		
		function drag(){
			x = Math.round((tx-sx)+sElx);
			y = Math.round((ty-sy)+sEly);
			
			if (kinetic){
				currentTime = new Date().getTime();
				if (timeControl+timerKin <= currentTime) {
					mouseMove = 2;
					timeControl = currentTime;
					lastX = tx;
					lastY = ty;
				}
			}
			
			if (winFrame) {
				xMax = Math.round(Ww-elW);
				yMax = Math.round(Wh-elH);
				if (x >= xMax){x = xMax;} if (x <= 0) {x = 0;}
				if (y >= yMax){y = yMax;} if (y <= 0) {y = 0;}
			}
			
			$(elemMove).css({'left' : x+'px','top':y+'px'})
		}
		
		if (typeEvent == 'touch') {
			$(elem).off('touchmove');
			$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
				if (event.targetTouches.length == 1) { // 1 касание
					touchm = event.targetTouches[0];
					tx = touchm.pageX;
					ty = touchm.pageY;
					drag();
				}
			}),false;

		}
		if (typeEvent == 'mouse') { 
			$(elem).off('mousemove');
			doc.onmousemove = function(event) {
				tx = event.clientX;
				ty = event.clientY
				drag();
			}
		}
		
		$(elem).off('touchend');$(elem).off('mouseup');
		$(elem).one('touchend mouseup',function(event) {
			$(elem).off('touchmove');$(elem).off('mousemove');
			doc.onmousemove = null;
			$(elem).onmousemove = null;
			var yRotaryControl = y;
			if (kinetic){ // kinetic move
				var xdif,xk,ydif,yk,kofX,kofY,xm,ym;
				var timeUp = new Date().getTime();
				
				xdif = sx-sElx;
				lastX = lastX-xdif;
				xk = x-lastX;
				
				ydif = sy-sEly;
				lastY = lastY-ydif;
				yk = y-lastY;
				
				var distanceX = xk*kofDis;
				var distanceY = yk*kofDis;
				
				//if (timeUp <= currentTime+200 && x < xMax && y < yMax && x > 0 && y > 0){ // отключение  отскакивания
				if (timeUp <= currentTime+200){
					animate({
						duration:timerAnim,
						timing: makeEaseOut(circ),
						draw: function(prog){
							xm = x+(prog*distanceX);
							ym = y+(prog*distanceY);
							if (winFrame){
								if (xm >= xMax) {xm = xMax;} if (xm <= 0) {xm = 0;}
								if (ym >= yMax) {ym = yMax;} if (ym <= 0) {ym = 0;}
							}
							$(elemMove).css({'left':xm,'top':ym});
						}
					});
				}
				yRotaryControl = y+distanceY;
			}
			if (rotaryL) {
				if (!yRotaryControl) {yRotaryControl = sEly;}
				if (yRotaryControl+(elH/2) >= rotaryLine) {
					if (resizeControl) {$(elem).find(rotaryBlock).css({'transform':'rotate('+rotaryHoriz+'deg)'});}
					else {$(elem).closest(rotaryBlock).css({'transform':'rotate('+rotaryHoriz+'deg)'});}
				}else {
					if (resizeControl) {$(elem).find(rotaryBlock).css({'transform':'rotate('+rotaryNone+'deg)'});}
					else {$(elem).closest(rotaryBlock).css({'transform':'rotate('+rotaryNone+'deg)'});}
				}
			}
		}),false;
	};
	
	// Resize Block
	function resize_block(data){
		var elem = data['elem'];
		var sx = data['sx'];
		var sy = data['sy'];
		var Ww = data['Ww']; // ширина главного окна
		var Wh = data['Wh']; // Высота главного окна
		var contentBlock = data['contentBlock'];
		var ew,eh,x,y,lx,ly,xMax,yMax,kofRes,resizeD;
		
		var elemMove = data['blockElem'];
		if (!elemMove){elemMove = elem;}
		
		
		if (contentBlock == 'img') {
			//kofRes = data['elH'] / data['elW'];
			//maxElemH = maxElemH*kofRes;
		}
		
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 2мя пальцами
			if (event.targetTouches.length == 2) {
				var touchA = event.targetTouches[0];
				var touchB = event.targetTouches[1];
				ew = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
				eh = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
				
				if (contentBlock == 'img') {
					resizeD = (eh+ew);
					x = resizeD+data['elW'];
					//y = (x*kofRes);
					//console.log(kofRes);
				}
				else {y = eh+data['elH']; x = ew+data['elW'];}
				
				lx = data['sElx']-(ew/2);
				ly = data['sEly']-(eh/2);
								
				if (x <= minElemW) {x = minElemW;} if (x >= maxElemW) {x = maxElemW;}
				if (y <= minElemH) {y = minElemH;} if (y >= maxElemH) {y = maxElemH;}
				
				xMax = Math.round(Ww-lx);
				yMax = Math.round(Wh-ly);

				if (lx <= 0) {lx = 0;} if (x >= xMax) {x = xMax;}
				if (ly <= 0) {ly = 0;} if (y >= yMax) {y = yMax;}

				$(elemMove).css({'height':y,'width':x});
				$(elemMove).css({'left' : lx+'px','top':ly+'px'});
			}
		}),false;
		
		$(elem).off('touchend');
		$(elem).one('touchend',function(event) {
			$(elem).off('touchmove');
			
		}),false;
	}
	
	// Rotary block
	function rotary_block(data){
		var elem = data['elem'];
		var sx = data['sx'];
		var sy = data['sy'];
		var ew,eh,x,y,rotate;
		var rotateO = 0;
		
		var elemMove = data['blockElem'];
		if (!elemMove){elemMove = elem;}
		
		$(elem).off('touchmove');
		$(elem).on('touchmove',function(event) { // если тянем 2мя пальцами
			if (event.targetTouches.length >= 3) {
				var touchA = event.targetTouches[0];
				var touchB = event.targetTouches[1];
				ew = Math.round(Math.abs(touchA.pageX - touchB.pageX)-sx);
				eh = Math.round(Math.abs(touchA.pageY - touchB.pageY)-sy);
				
				x = ew+data['elW'];
				y = eh+data['elH'];
				rotate = x-y;
				
				//elem.rotate(rotate);
				if (rotate >= 90) {rotateO = 0;}
				if (rotate <= 90) {rotateO = 180;}
				if (rotate >= -90) {rotateO = 0;}
				if (rotate <= -90) {rotateO = 180;}
				$('#info1').html('rotate = '+rotate+'<br> y = '+y)
				$(elemMove).css({'transform': 'rotate('+rotate+'deg)'});
				//$(elem).animate({'transform': 'rotate('+rotateO+'deg)'});
				//$(elem).css({'left' : lx+'px','top':ly+'px'});
			}
		}),false;
		
		$(elem).off('touchend');
		$(elem).one('touchend',function(event) {
			$(elem).off('touchmove');
			
			$(elemMove).css({'transform': 'rotate('+rotateO+'deg)'});
			
		}),false;
	}

	// touch block
	$(windo).on('touchstart',blockTarget,function(event) { // на все блоки blockTarget повешаем эвент (даже если их нету)
		var timeStart = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var elXY = blockElem.position();
		var sElx = elXY.left; // расположение ЛЕФТ элемента
		var sEly = elXY.top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(),contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}
		
		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'touch'
		};
		
		if (tt == 1) {
			if (control2C != 1) {//двойной клик
				var GlobObject = gDataInfo['block_'+index];
				if (GlobObject){
					if (GlobObject.startTime+timerDB >= timeStart) {
						var heightTE = $(elem).css('height');
						if (heightTE == targetCotrolH+'px'){
							$(elem).animate({"height": 100+'%'},timerAnimBut);
							$(elem).children(targetOpen).html('&uarr;');
						}
						else {
							$(elem).animate({"height": targetCotrolH+'px'},timerAnimBut);
							$(elem).children(targetOpen).html('&darr;');
						}
					}
					gDataInfo['block_'+index] = {'startTime':timeStart};
				}
				else {gDataInfo['block_'+index] = {'startTime':timeStart};}
			}	
			
			var touch = event.targetTouches[0];
			data['sx'] = touch.pageX;
			data['sy'] = touch.pageY;
			
			drag_block(data);
		}
		
		else if (tt == 2) {
			var touchSA = event.targetTouches[0];
			var touchSB = event.targetTouches[1];
			data['sx'] = Math.round(Math.abs(touchSA.pageX - touchSB.pageX));
			data['sy'] = Math.round(Math.abs(touchSA.pageY - touchSB.pageY));
			
			resize_block(data);
		}

		else if (tt >= 3) {	
			var touchSA = event.targetTouches[0];
			var touchSB = event.targetTouches[1];
			data['sx'] = Math.round(Math.abs(touchSA.pageX - touchSB.pageX));
			data['sy'] = Math.round(Math.abs(touchSA.pageY - touchSB.pageY));
			
			//rotary_block(data);
		}
	}),false;
	
	
	// mouse
	$(windo).on('mousedown',blockTarget,function(event) {
		var timeStart = new Date().getTime(); // время клика на объект
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var elXY = blockElem.position();
		var sElx = elXY.left; // расположение ЛЕФТ элемента
		var sEly = elXY.top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(),contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}

		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'mouse'
		};
		
		if (control2C != 1) {//двойной клик
			var GlobObject = gDataInfo['block_'+index];
			if (GlobObject){
				if (GlobObject.startTime+timerDB >= timeStart) {
					var heightTE = $(elem).css('height');
					if (heightTE == targetCotrolH+'px'){
						$(elem).animate({"height": 100+'%'},timerAnimBut);
						$(elem).children(targetOpen).html('&uarr;');
					}
					else {
						$(elem).animate({"height": targetCotrolH+'px'},timerAnimBut);
						$(elem).children(targetOpen).html('&darr;');
					}
				}
				gDataInfo['block_'+index] = {'startTime':timeStart};
			}
			else {gDataInfo['block_'+index] = {'startTime':timeStart};}
		}	
		
		data['sx'] = event.clientX;
		data['sy'] = event.clientY;

		drag_block(data);
	});
	
	
	// Delete block
	$(windo).on('click touchstart',blockDelete,function(event) {
		$(this).closest(block).remove()
	});
	
	
	// Open Control Menu
	$(windo).on('click touchstart',targetOpen,function(event) {
		var elem = $(this).closest(blockTarget); // получим основной движемый элемент	
		var heightTE = $(elem).css('height');	
		if (heightTE == targetCotrolH+'px') {
			$(elem).animate({"height": 100+'%'},timerAnimBut);
			$(this).html('&uarr;');
		}
		else {
			$(elem).animate({"height": targetCotrolH+'px'},timerAnimBut);
			$(this).html('&darr;');
		}
	});
	
	
	// mini button click
	$(butСС).on('click touchstart', function(event) {
		var winCont = $(this).closest(windowControl)
		var topWin =  $(winCont).position().top;
		var lefWin = $(winCont).position().left;
		var cloneInfo = $(this).find(cloneContent);
		var dataInfo = cloneInfo.data();
		var elemTop=0,elemLeft=0,specClass='block-object',pastContent,cloneElem,RBC,border,background,idNamme,createBlock;

		elemTop = (dataInfo.top) ? (dataInfo.top+topWin):0;
		elemLeft = (dataInfo.left) ? (dataInfo.left+lefWin):0;

		
		if (dataInfo.type) {
			if (dataInfo.rotary) {RBC = dataInfo.rotary;}
			else {RBC = '0';}
			if (dataInfo.type == 'content') {
				createBlock = true;
				pastContent = $(this).find(cloneInfo).html();
			}
			else if (dataInfo.type == 'img') {
				if (dataInfo.src) {
					specClass = 'block-img';
					pastContent = '\
						<div class="block '+specClass+'" style="top:'+elemTop+'px; left:'+elemLeft+'px;" data-control2c="1" data-content="img">\
							<div class="block-rotary" style="transform: rotate('+RBC+'deg);">\
								<div class="block-target">\
									<img src="'+dataInfo.src+'">\
									<div class="block-delete">×</div>\
								</div>\
							</div>\
						</div>\
					';
				}
			}
			else if (dataInfo.type == 'iframe') {
				if (dataInfo.src) {
					createBlock = true;
					pastContent = '<iframe src="'+dataInfo.src+'"></iframe>';
				}
			}
			else if (dataInfo.type == 'video'){
				createBlock = true;
				pastContent = ' <video controls="controls">\
									<source src="'+dataInfo.src+'">\
								</video>'
			}
			else if (dataInfo.type == '3d') {
				createBlock = true;
				id3D++;
				idNamme = 'canvas3d_'+id3D;
				pastContent = '<div class="canvas3d" id="'+idNamme+'"></div>';
				
			}
		}
		
		if (dataInfo) {
			if (dataInfo.border) {border = 'border:'+dataInfo.border+';';} else {border="";}
			if (dataInfo.back) { background = 'background:'+dataInfo.back+';';}else {background="";}
		}
		
		if (createBlock == true) {
			cloneElem = '<div class="block '+specClass+'" style="top:'+elemTop+'px; left:'+elemLeft+'px; '+border+' '+background+'">\
			<div class="block-rotary" style="transform: rotate('+RBC+'deg);">\
				<div class="block-target">\
					<div class="open-control-menu">&darr;</div>\
					<div class="block-delete">&times;</div>\
				</div>\
				<div class="blok-content">\
			'+pastContent+'</div></div></div>';
			$(windo).append(cloneElem);
		
			if (dataInfo.type == '3d' && dataInfo.src) {Load_Canvas_3D(idNamme,dataInfo.src);}
		}
		else {$(windo).append(pastContent);}
	});
	
	
	// background buttun
	$(butBС).on('click touchstart', function(event) {
		var cloneInfo = $(this).find(cloneContent);
		var dataInfo = cloneInfo.data();
		var target = $(this).closest(windowControl)
		
		//console.log(dataInfo);
		//console.log(backAdd);
		
		if (dataInfo.type) {
		var bacSrc = dataInfo.src;
			if (dataInfo.type == 'background') {
				var background = $(target).css('background-image');
				if ( $(target).css('background-image') != 'none' ){
					$(target).css({'background-image':'none'});
				}
				else {
					$(target).css({'background-image':'url('+bacSrc+')'});
				}
			}
		}
	});
	
	
	// central button  cenButCli centralBut
	$(cenButCli).on('click touchstart', function(event) {
		var elem = $(this).closest(centralBut);
		var posBot = $(elem).css('bottom');
		
		if (posBot == dropUp+'px') {  //опускаем
			$(elem).animate({"bottom": dropDown+'px'},ContTimAnim,"linear",function(){
				$(elem).find('.centr-mini-but').css({'display':'block'});
				$(elem).find('.centr-mini-but').animate({'opacity': '1.0'},ContTimAnim,"linear");
			});
		}
		else { //поднимаем
			$(elem).find('.centr-mini-but').animate({'opacity': '0.0'},ContTimAnim,"linear",function(){
				$(elem).find('.centr-mini-but').css({'display':'none','opacity': '0.0'});
			});
			$(elem).delay(ContTimAnim).animate({"bottom": dropUp+'px'},ContTimAnim,"linear");
			
		}
	});
});

