var doc = document;
var winFrame = true; // если правда блок небудет выходить за рамки, своего родителя, если лож будет
var kinetic = true; // Физика движений для блоков, включить выключить.
//var rotaryL = false; // включение переворачивания блоков после линии
var rotaryL = true; // включение переворачивания блоков после линии
var rotaryRevers = true; // включить обратное отображение блоков ( зеркально реверсное )
var disapText = true; // включить/выключить исчезновение текста (после появления ) 


// главное окно 
var windo = '.content'; // клас элемента окна, в котором движущиеся блоки (весь экран)
	var fulWindowWidth = 1920; // ширина всего экрана
	var fulWindowHeight = 1080; // высота всего экрана
// - главное окно -

// окна управления (рабочии зоны)
var windowControl = '.window'; // класс ОКОН внутри которых расположена кнопка управления
	var allWindowCo = 4; // общее количество окон упровления
	var autoWindowCo = true; // автоматически выстраивать, в зависемости от объекта построения страници
	var lineWindowCo = 2; // количество линий построения
	var winWidthCo = 960; // ширина окна
	var winHeightCo = 540; // высота окна
// - окна управления (рабочии зоны) -	

var block = '.block'; // класс элемента который двигается
var blockTarget = '.block-target'; // класс элемента с помощью которого двигаем
var blockResize = '.block-resize'; // класс элемента с помощью меняем размер блока
var blockDelete = '.block-delete'; // класс при клики на который удоляем блок

var blockOpen = '.block-open'; // класс который раскрывается с 0% до 100%
var targetOpen = '.open-control-menu'; // класс при клике на который раскрывается blockOpen c 0% до 100%
var typeOpen = 'right' //  пит открытия меню "right" - с право на лево "top" - сверху вниз
var arreyOpen = '&larr;'; // кнопка открытия меню ресайза
var arreyClos = '&rarr;'; // кнопка закрытия меню ресайза


var butСС = '.but-create-content'; // кнопка создания новыхх блоков 
var butBС = '.but-background-content'; // кнопка изменения фонового изображения
var cloneContent = '.clone-content'; // все что внутри этого блока клонировать на главный экран (или по дата информации)




var blokContent = '.blok-content';// класс внутри которого распалагается контент (внутри блока который)


var conInfoWin = '.content-info-win'; // класс окна внутри которого скролится conInfoMove
var conInfoMove = '.content-info-move'; // класс внутри которого есть контент, скролим вверх в низ, внутри упровляемого блока

// 3Д . классы внутри 3Д
var canvas3d = '.canvas3d'; //  клаасс в котором будет прогружен 3D контент
var BCDopWindow = '.blok-content_dopwindow'; // клас дополнительно появляющегося окна после нажатия на 3Д объект
var dopInf3dExit = 'left'; //  с какой стороны будет ВЫЕЖАТЬ окно под инфы внутри 3Д, 1)false(в зависемости от нажатия на какую из половин экрана) 2)left(вегда с лева) 3)right(вегда с право) 
var BCLocking = '.blok-content_locking'; // блокирующий упровления 3Д объекта
var back3d = '.back3d'; // класс по которому возврощаем 3D к исходному положэению
var back3dBut = '.back3dBut'; // класс отвечающий за визуальный вид кнопки ( Назад к 3д)
var back3dInf = '×'; // то что в нутри кнопки
var widthDopInf = 70;  // % на который откроется дополнительное окно после нажатие на 3Д элемент
var opBCLocking= 0.2; // прозрачность фона накладываемого на 3Д после открытия доп контенте
var blurBCLocking = 2;// размытие 3Д модели после открытия доп контента
var swipeX3d = 100; // количество пикселей для отслеживания свайпа по Х (горизонтали width)
// - 3Д . классы внутри 3Д -

// центральная кнопка
var centralBut = '.central-but'; // центральная кнопка на дисплее
var cenButCli = '.central-but-click'; // блок на который нажимаем для работы центральной кнопки
	var dropDown = -23; // при клике опускать на это виличену
	var dropUp = 80; // при клике поднимать на это виличену (начальное расположение кнопки)
	var ContTimAnim = 400; // время анимаций контрольной кнопки
	var ContTimAnimMin = 800; // время появления мини кнопочек
	
var returnButton = true; // включить выключить возврат кнопки
	var timeReclame = 10*1000; //  время через которое запускается возврат кнопки

var clCenMinBut = '.centr-mini-but'; // класс внутри которого рапологаются кнопки (что создают блоки)
var clCreConInfo = '.create-content-info'; //класс отвечающий за визуальный вид мини кнопки
var showDopInfText = '.show-dop-info-text'; // класс в котором появляется текст над кнопками а затем исчезает
	var showDopInfTextTime = 2000; // время через которое исчезает дополнительный текс
	var showDopInfTextTimeOp = 500; // время в течении которого исчезает дополнительный текст
//	- центральная кнопка -

// rotary line block
var rotaryBlock = '.block-rotary'; // блок внутри которого все перевернется
var rotaryLine = 539; // высота отнасительно TOP после которой блок перевернется
var rotaryNone = '0'; // поворот блока при создании
var rotaryHoriz = 180; // горинзонтальный поворот

var timerDB = 200; // таймер между кликами (для дабл клика)
var timerKin = 200; // таймер для инерции (время в которое фиксируется растояние пройденное пальцем) 
var timerAnim = 400; // таймер Анимации физики элемента (время которое летит элемент после броска)(3D расскрытие закрытие)
var timerAnimBut = 200; // таймер Анимации Кнопок (время раскрывания закрывания кнопок)

var kofDis = 2; // коофицент дистанции при кинематике ( растояние которое пролетит блок после отпускания )
var minElemW = 180; // минимальная ширина элемента
var minElemH = 180; // минимальная высота элемента
var maxElemW = 900; // максимальная высота элемента
var maxElemH = 900; // максимальная высота элемента


var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
var gDataInfo = {}; // Объект с глобальными переменными
var id3D = 1; // глобальная переменная для отслеживания ИД 3д объекта


//имена объектов что внутри 3Д сцены, И HTML  которы нужно вывести на экран после клика
var objCon3dButAntiFatal = 'b_red'; // важное, по этому имени проверяется есть ли в сцена объекты на которые мы можем нажимать
var objCon3dBut = {
	//'b_red':'<p> Привет я Красный Кубик </p> <p><img src="img/reb_c_min.jpg"></p> <p><img src="img/As5text.png"></p>',
	'b_red':'<p><img src="img/As5text.png"></p>',
	'b_gren':'<p> Привет я Зеленый Кубик </p> <p><img src="img/gre_c_min.jpg"></p> <p><img src="img/As5text.png"></p>',
	'b_blue':'<p> Привет я Синий Кубик </p> <p><img src="img/blu_c_2_min.jpg"></p> <p><img src="img/As5text.png"></p>'
};

// Объект для создания страници
var objectBlock = {			
		'top':0, // расположение кнопки сверху
		'left':0, // расположение кнопки слева
		'show':0, // путь до картинки для обложки кнопки
		'showtext':{'text':'info','left':0,'top':0}, // текст который появляется над кнопками(и его положение), и затем исчезает 
		'type':0, // тип кнопки ( какой объект создать на поле )
		'src':0, // ссылка на контент который нужно создать в объекте ( или сам контент )
		'rotary':false, // поворот создоваемого объекта ( если false будет использоваться поворот окна в котором кнопка )
		'position': {'top':0,'left':0}, // положение ново-созданного объекта на поле ( относительно окна в котором он )
		'border':0, // css свойство border 
		'back':0} // css свойство background


var ob_1 = {
		'top':-60,
		'left':-85,
		'show':'<div class="shine mini-but-s"></div>',
		'showtext':{'text':'Объект-1<div class="arrow-text-right"></div>','left':-80,'top':-40},
		'type':'3d',
		'src':'3D/Diag_ASU.json',
		'rotary':false,
		'position': {'top':1,'left':1},
		'border':'none'
	}
	
var ob_2 = {
		'top':-80,
		'left':-10,
		'show':'<div class="shine mini-but-s"></div>',
		'showtext':{'text':'Объект-2<div class="arrow-text-top_big"></div>','left':1,'top':-48},
		'type':'3d',
		'src':'3D/testTauch.json',
		'rotary':false,
		'position': {'top':1,'left':280},
		'border':'none'
	}

var ob_3 = {
		'top':-60,
		'left':65,
		'show':'<div class="shine mini-but-s"></div>',
		'showtext':{'text':'Объект-3<div class="arrow-text-left"></div>','left':89,'top':-40},
		'type':'3d',
		'src':'3D/testTauch.json',
		'rotary':false,
		'position': {'top':1,'left':560},
		'border':'none'
	}
		
var ob_4 = {
		'top':-60,
		'left':380,
		'show':'<div class="shine_red mini-but-s"></div>',
		'showtext':{'text':'Объект-4<div class="arrow-text-top"></div>','left':0,'top':-20},
		'type':'3d',
		'src':'3D/testTauch.json',
		'rotary':false,
		'position': {'top':1,'left':300},
		'border':'none'
	}
	
var objects = {
	'ob_1':ob_1,
	'ob_2':ob_2,
	'ob_3':ob_3,
	'ob_4':ob_4
}
	
var windowCreate = {
	'window_1':{
		'top':0,
		'left':0,
		'objects':objects
	},
	'window_2':{
		'top':0,
		'left':959,
		'objects':objects
	},
	'window_3':{
		'top':539,
		'left':0,
		'objects':objects
	},
	'window_4':{
		'top':539,
		'left':959,
		'objects':objects
	}
};



