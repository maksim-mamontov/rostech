var doc = document;
var createPage = true; // Включить выключить автоматическое создание страници
var winFrame = true; // если правда блок небудет выходить за рамки, своего родителя, если лож будет
var kinetic = true; // Физика движений для блоков, включить выключить.
//var rotaryL = false; // включение переворачивания блоков после линии
var rotaryL = true; // включение переворачивания блоков после линии
var rotaryRevers = true; // включить обратное отображение блоков ( зеркально реверсное )
var disapText = true; // включить/выключить исчезновение текста (после появления ) 
var returnButton = false; // включить выключить возврат кнопки
var commerc = true; //если включено показывает рекламу если система неактивна

// дебагные штуки
var view3dObjectName = false; // отображает имя 3Д объекта в сцена ( если мы на него кликнули ) 
// - дебагные штуки -

// Реклама
var commercTime = 60*60*1000; //время котороя система должна быть неактивна
var comClassMid = '.commercial'; // главный клас по которому отслеживаем смещение рекламы
var comClass = '.commercial-window'; // класс который появляется, и исчезает при неактивной системе
var comClassCont = '.commercial-window-cont'; // класс с помощью которого меняем контент рекламы
//контент который будет появляться в рекламнойм блоке. time - время появления. cont - сам контент
var comContent = {
	0:{
		'time':60*1000,
		'cont':'<img src="img/back_gif.gif" width="100%" height="100%">',
	},
	1:{
		'time':105*1000,
		'cont':'<video src="video/RosTeh_Master_nofocus_NoLoop.mp4" width="100%" height="100%" autoplay="autoplay" loop="loop" muted></video>',
	},
};
// - Реклама -

// главное окно 
var disableTouchClass = '.disable-touch'; // класс по которому отключаем все тачь действия по умолчанию 
var windo = '.content'; // клас элемента окна, в котором движущиеся блоки (весь экран)
	//var fulWindowWidth = 3840; // ширина всего экрана
	//var fulWindowHeight = 2160; // высота всего экрана
	//var fulWindowTypeSize = 'px'; // то в чем выставлены размеры страници
	
	var fulWindowWidth = 100; // ширина всего экрана
	var fulWindowHeight = 100; // высота всего экрана
	var fulWindowTypeSize = '%'; // то в чем выставлены размеры страници
// - главное окно -

// окна управления (рабочии зоны)
var windowControl = '.window'; // класс ОКОН внутри которых расположена кнопка управления
	var lineWindowCo = 2; // количество линий построения
	var allWindowCo = 2; // общее количество окон упровления
	// (*СУПЕР приоритет, если 0 будет строит от объекта снизу windowCreate)
	//	если существует будет строится от объекта "objectsButton"
	//  если allWindowCo - сущестует, все что ниже неимеет смысла
	var autoWindowCo = true; // автоматически выстраивать, в зависемости от объекта построения страници
	var winWidthCo = 960; // ширина окна
	var winHeightCo = 540; // высота окна
// - окна управления (рабочии зоны) -	

// упровляемые блоки на поле
var block = '.block'; // класс элемента который двигается
var blockActive = '.block-active'; // класс элемента который двигается
var blockTarget = '.block-target'; // активный класс когда кликнули по блоку
var blockTargetV = '.block-target-v'; // визуальный вид блок таргета (не активного)
var blockTargetVA = '.block-target-va'; // визуальный вид блок таргета (активного)
var blockResize = '.block-resize'; // класс элемента с помощью которого меняем размер блока
var blockDelete = '.block-delete'; // класс при клики на который удоляем блок
var bloсkContent = '.blok-content';// класс внутри которого распалагается контент (внутри блока который)
var rotaryBlock = '.block-rotary'; // блок внутри которого все перевернется
var blockWidth = 800; // ширина ново создоваемого блока
var blockHeight = 640; // высота новосоздоваемого блока
// - упровляемые блоки на поле -

// дополнительный блок управления
var blockOpen = '.block-open'; // класс который раскрывается с 0% до 100%
var blockOpenImg = 'img/control_info_help_size_1.png'
var targetOpen = '.open-control-menu'; // класс при клике на который раскрывается blockOpen c 0% до 100%
var typeOpen = 'right' // открытие меню blockOpen "right" - с право на лево "top" - сверху вниз
var arreyOpen = '&larr;'; // кнопка открытия меню ресайза
var arreyClos = '&rarr;'; // кнопка закрытия меню ресайза
var cloneContent = '.clone-content'; // все что внутри этого блока клонировать на главный экран (или по дата информации)
// - дополнительный блок управления -

// кнопки в окне управления
var centralBut = '.central-but'; // центральная кнопка на дисплее
var cenButCli = '.central-but-click'; // блок на который нажимаем для работы центральной кнопки
	var dropDown = 40; // при клике опускать на это виличену
	var dropUp = 140; // при клике поднимать на это виличену (начальное расположение кнопки)
	var ContTimAnim = 400; // время анимаций контрольной кнопки
	var ContTimAnimMin = 800; // время появления мини кнопочек
	var ContTimDelayClickDp = 5000; // время задержки между кликами после ОПУСКАНИЯ кнопки
	var ContTimDelayClickUw = 1500; // время задержки между кликами после ПОДНЯТИ кнопки
	
var clCenMinBut = '.centr-mini-but'; // класс внутри которого рапологаются кнопки (что создают блоки)
var clCreConInfo = '.create-content-info'; //класс отвечающий за визуальный вид мини кнопки
var showDopInfText = '.show-dop-info-text'; // класс в котором появляется текст над кнопками а затем исчезает
	var showDopInfTextClick = true; // сделать текст нажимаемым "showDopInfText", (нажали создался блок) 
	var showDopInfTextTime = 4000; // время через которое исчезает дополнительный текс
	var showDopInfTextTimeOp = 500; // время в течении которого исчезает дополнительный текст
	
var returnButtonTime = 10*1000; //  время через которое запускается возврат кнопки	
var butСС = '.but-create-content'; // кнопка создания новыхх блоков 
	var butCCpos = 'button'; // относительно чего создовать новые блоки. 
		//window - относительно окна управления 
		//button - относительно центральной кнопки
var butBС = '.but-background-content'; // кнопка изменения фонового изображения
var butBBС = '.but-background-content-but'; // кнопка изменения фонового изображения (для всплывающего текста)

var HelpWind = '.help-wind'; // дополнительное окно поверх рабочей зоны
var HelpViewWind = '.help-view-wind'; // класс отвечающий за внешний вид
	var HelpWindContent = '.help-wind-content';// окно контента внутри хелпа (на нем анимация)
	var HelpWindViewContent = '.help-wind-View-content';// окно визуальный вид
	var HelpWindViewContentSiz = '.help-wind-content-size';// окно по размерам внутри окна хелпа
		HWVSizW = 980; // размеры Ширены дополнительно окна хелпа
		HWVSizH = 1080; // размеры Ширены дополнительно окна хелпа
	var HelpWindContentCont = '<img src="img/control_info_help_7.png">'; //то что будет в окне контента
	var HelpWindViewContentPos = 'bot'; // откуда раскрывается окно
	var helpWindowWidth = 980; // ширина окна хелпа
	//var helpWindowHeight = '100%'; // высота окна хелпа
	var HelpWindContentTime = 3000; // время появления окна контента хелпа
	var HelpWindContentTimeShad = 1000; // время исчезновения окна контента хелпа
var butHelpWind = '.but-help-info'; // Кнопка при нажатии на которую открывается окно поверх рабочей зоны
var butHelpViewWind = '.but-help-viwe-info'; // внешний вид кнопки хелпа
var butHelpCloWind = '.but-help-сlose-info'; // Кнопка закрытия зоны поверх рабочей зоны
var butHelpCloViewWind = '.but-help-сlosew-info'; // внешний вид кнопки закрытия
// - кнопки в окне управления -


var conInfoWin = '.content-info-win'; // класс окна внутри которого скролится conInfoMove
var conInfoMove = '.content-info-move'; // класс внутри которого есть контент, скролим вверх в низ, внутри упровляемого блока

// 3Д . классы внутри 3Д
var canvas3d = '.canvas3d'; //  клаасс в котором будет прогружен 3D контент
var BCDopWindow = '.blok-content_dopwindow'; // клас дополнительно появляющегося окна после нажатия на 3Д объект
var dopInf3dExit = 'left'; //  с какой стороны будет ВЫЕЖАТЬ окно под инфы внутри 3Д, 1)false(в зависемости от нажатия на какую из половин экрана) 2)left(вегда с лева) 3)right(вегда с право) 
var BCLocking = '.blok-content_locking'; // блокирующий упровления 3Д объекта
var back3d = '.back3d'; // класс по которому возврощаем 3D к исходному положэению
var back3dBut = '.back3dBut'; // класс отвечающий за визуальный вид кнопки ( Назад к 3д)
var back3dInf = '×'; // то что в нутри кнопки
var widthDopInf = 70;  // % на который откроется дополнительное окно после нажатие на 3Д элемент
var opBCLocking= 0.2; // прозрачность фона накладываемого на 3Д после открытия доп контенте
var blurBCLocking = 2;// размытие 3Д модели после открытия доп контента
var swipeX3d = 100; // количество пикселей для отслеживания свайпа по Х (горизонтали width)
// управление контроля от случайных нажатий, при листании и манепулирование 3Д
var timer3dControl = true; // определяет нужно ли проверять двигаем или кликаем(включить системы анти случайный клик)
var timer3dControlEnd = false; // проверка по отпусканию пальца
var timer3dControlrandkof = 5; // случайного сдвига пальца
var timeRandomClick = 200; // время через которое система анализирует былоли действие кликом (по внутреннему 3д элементу) или же это было просто движение пальцем. если было движение пальцем 3Д элемент ненажмется
// - 3Д . классы внутри 3Д -



// rotary line block
var rotaryLine = 539; // высота отнасительно TOP после которой блок перевернется
var rotaryLine = fulWindowHeight/2; // высота отнасительно TOP после которой блок перевернется
var rotaryNone = '0'; // поворот блока при создании
var rotaryHoriz = 180; // горинзонтальный поворот

var timerDB = 200; // таймер между кликами (для дабл клика)
var timerKin = 200; // таймер для инерции (время в которое фиксируется растояние пройденное пальцем) 
var timerAnim = 400; // таймер Анимации физики элемента (время которое летит элемент после броска)(3D расскрытие закрытие)
var timerAnimBut = 200; // таймер Анимации Кнопок (время раскрывания закрывания кнопок)

var kofDis = 4; // коофицент дистанции при кинематике ( растояние которое пролетит блок после отпускания )
var minElemW = 180; // минимальная ширина элемента
var minElemH = 180; // минимальная высота элемента
var maxElemW = 1800; // максимальная ширина элемента
var maxElemH = 1350; // максимальная высота элемента

// окно управления Админка (вызывается нажатие Ctrl + Q)
var adminControlWindow = true; // включить панель управления администратора
var adminControlWindowClass = '.admin-control-window'; // класс панели управления администратора
var adminControlWindowClassFO = '.admin-control-window-open-one'; // класс при нажатии (1 раз) на который вызываем админ меню
var adminControlWindowClassF = '.admin-control-window-open'; // класс при нажатии (зависит от опции ниже) на который вызываем админ меню
var adminControlWindowFinger = 4; // количество пальцев для вызова, или быстрых кликов админ панели
var adminControlWindowTimer = 1000; // время в интервал которого нужно успеть сделать клики
var adminControlWindowTimerAnim = 500; // время появления меню
var adminControlWindowContent = ''+
'<p><a href="/persone_2.php">x 2</a></p>'+ // класс панели управления администратора
'<p><a href="/persone_4.php">x 4</a></p>'+ // класс панели управления администратора
'<p><a href="/persone_6.php">x 6</a></p>'+ // класс панели управления администратора
'<p><a href="/persone_8.php">x 8</a></p>'+ // класс панели управления администратора
'<p><a href="/persone_1.php">x VIP</a></p>'; // класс панели управления администратора
// - окно управления -


// Vip кнопки и т.д
var vipRotateBut = '.vip-rotate-but'; // класс кнопки при нажатии на которую происходит переворота экрана 
var vipRotateButDop = '.vip-rotate-but-dop'; // кнопка переворота сверху
var vipDopHelpClass = '.vip-dop-help-class';// пополнительный класс для хелпа в вип режиме
	var VIPbiasBut = 25; // смещение кнопки в вип режиме на %
// - Vip кнопки и т.д -

var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
var gDataInfo = {}; // Объект с глобальными переменными
var id3D = 1; // глобальная переменная для отслеживания ИД 3д объекта


//имена объектов что внутри 3Д сцены, И HTML  которы нужно вывести на экран после клика
var objCon3dButAntiFatal = 'control_element'; // важное, по этому имени проверяется есть ли в сцена объекты на которые мы можем нажимать
var objCon3dBut = {
	// АСУ 
	'asup':asu_asu,
	'asup_1':asu_asu,
	'anal_sistem':asu_anal_sistem,
	'anal_sistem_1':asu_anal_sistem,
	'erp':asu_erp,
	'erp_1':asu_erp,
	'mrp':asu_erp,
	'mrp_1':asu_erp,
	'scm':asu_erp,
	'scm_1':asu_erp,
	'virt_kb':asu_virt_kb,
	'virt_kb_1':asu_virt_kb,
	'mes_1':asu_mes,
	'mes':asu_mes,
	'asu_tp_l':asu_asu_tp,
	'asu_tp_l_1':asu_asu_tp,
	'asu_tp_r_1':asu_asu_tp,
	'asu_tp_r':asu_asu_tp,
	'cod':asu_cod,
	'cod_1':asu_cod,
	'sensors':asu_smart_dev,
	'sensors_1':asu_smart_dev,
	'sensors_3':asu_smart_dev,
	'sensors_4':asu_smart_dev,
	'sensors_a':asu_smart_dev,
	'smart_dev':asu_smart_dev,
	'smart_dev_1':asu_smart_dev,
	'llot_net':asu_llot,
	'llot_net_1':asu_llot,
	// - АСУ -
	// УМНЫЙ ГОРОД 
	'smart_city':ug_ug,
	'smart_city_1':ug_ug,
	'integration':ug_ug_integration,
	'integration_1':ug_ug_integration,
	'turism':ug_ug_turism,
	'turism_1':ug_ug_turism,
	'smart_zhkh':ug_ug_smart_zhkh,
	'smart_zhkh_1':ug_ug_smart_zhkh,
	'smart_light':ug_ug_smart_light,
	'smart_light_1':ug_ug_smart_light,
	'resource_acc':ug_ug_resource_acc,
	'resource_acc_1':ug_ug_resource_acc,
	'smktts':ug_ug_smktts,
	'smktts_1':ug_ug_smktts,
	'smart_trans':ug_ug_smart_trans,
	'smart_trans_1':ug_ug_smart_trans,
	'eekt':ug_ug_eekt,
	'eekt_1':ug_ug_eekt,
	'sopp':ug_ug_sopp,
	'sopp_1':ug_ug_sopp,
	'svdd':ug_ug_svdd,
	'svdd_1':ug_ug_svdd,
	'svnpdd':ug_ug_svnpdd,
	'svnpdd_1':ug_ug_svnpdd,
	'svgk':ug_ug_svgk,
	'svgk_1':ug_ug_svgk,
	'ekomonit':ug_ug_ekomonit,
	'ekomonit_1':ug_ug_ekomonit,
	'safety_city':ug_ug_safety_city,
	'safety_city_1':ug_ug_safety_city,
	'smuchs':ug_ug_smuchs,
	'smuchs_1':ug_ug_smuchs,
	'sirena':ug_ug_sirena,
	'sirena_1':ug_ug_sirena,
	'ecor':ug_ug_ecor,
	'ecor_1':ug_ug_ecor,
	'video':ug_ug_video,
	'video_1':ug_ug_video,
	'user_inter':ug_ug_user_inter,
	'user_inter_1':ug_ug_user_inter,
	// - УМНЫЙ ГОРОД -
	// Электронное здравоохранение
	'ministerstvo_1':ug_ug_ministerstvo,
	'egisz':ug_ug_egisz,
	'egisz_1':ug_ug_egisz,
	'rmo_dop_1':rmo_dop_1,
	'rmo_dop_2':rmo_dop_2,
	'rmo_dop_3':rmo_dop_3,
	'rmo_dop_4':rmo_dop_4,
	'fgbo':ug_ug_fgbo,
	'fgbo_1':ug_ug_fgbo,
	'rmo':ug_ug_rmo,
	'rmo_1':ug_ug_rmo,
	'fgbo_dop_1':fgbo_dop_1,
	'fgbo_dop_2':fgbo_dop_2,
	'fgbo_dop_3':fgbo_dop_3,
	'fgbo_dop_4':fgbo_dop_4,
	'fgbo_dop_5':fgbo_dop_5,
	'region_sigment':ez_region_sigment,
	'info_space':ez_info_space,
	'info_space_1':info_space_1,
	'info_space_2':info_space_2,
	'info_space_3':info_space_3,
	'info_space_4':info_space_4,
	'info_space_5':info_space_5,
	'info_space_6':info_space_6,
	'mzrf':ez_mzrf,
	'mzrf_1':ez_mzrf,
	//  - Электронное здравоохранение -
	//'___':___,
};

// Объект для создания страници
var objectBlock = {			
		'top':0, // расположение кнопки сверху
		'left':0, // расположение кнопки слева
		'show':0, // путь до картинки для обложки кнопки
		'showtext':{'text':'info','left':0,'top':0}, // текст который появляется над кнопками(и его положение), и затем исчезает 
		'type':0, // тип кнопки ( какой объект создать на поле )
		'src':0, // ссылка на контент который нужно создать в объекте ( или сам контент )
		'rotary':false, // поворот создоваемого объекта ( если false будет использоваться поворот окна в котором кнопка )
		'position': {'top':0,'left':0}, // положение ново-созданного объекта на поле ( относительно окна в котором он )
		'border':0, // css свойство border 
		'back':0} // css свойство background
// кнопка переворачивания экрана для вид пользования 
var butRotateVip = {
		'top':-60,
		'left':-300,
		'show':'<div class="shine mini-but-s"></div><img src="img/icon/rotate-arrow_mini.png">',
		'showtext':{'text':'Переворот<div class="arrow-text-top"></div>','left':0,'top':-20},
	}

var ob_1 = {
		'top':-60,
		'left':-85,
		'show':'<div class="shine mini-but-s"></div>',
		'showtext':{'text':'Электронное здравоохранение<div class="arrow-text-right"></div>','left':-78,'top':-60},
		'type':'3d',
		'src':'3D/ez/Diag_EZ8.json',
		'rotary':false,
		'position': {'top':-800,'left':-450},
		'border':'none'
	}
	
var ob_2 = {
		'top':-80,
		'left':-10,
		'show':'<div class="shine mini-but-s"></div>',
		'showtext':{'text':'Умный город<div class="arrow-text-top_big"></div>','left':3,'top':-61},
		'type':'3d',
		'src':'3D/ug/Diag_UG10zaebe.json',
		'rotary':false,
		'position': {'top':-900,'left':-375},
		'border':'none'
	}

var ob_3 = {
		'top':-60,
		'left':65,
		'show':'<div class="shine mini-but-s"></div>',
		'showtext':{'text':'АСУ в производстве<div class="arrow-text-left"></div>','left':89,'top':-60},
		'type':'3d',
		'src':'3D/asu/Diag_ASU_color3.json',
		'rotary':false,
		'position': {'top':-800,'left':-300},
		'border':'none'
	}
		
var ob_4 = {
		'top':-60,
		'left':380,
		'show':'<div class="shine_red mini-but-s"></div>',
		'showtext':{'text':'Город<div class="arrow-text-top"></div>','left':0,'top':-20},
		'type':'3d',
		'src':'3D/sity/Smart_City_Test_Cubes6.json',
		'rotary':false,
		'position': {'top':-900,'left':-375},
		'border':'none'
	}
var ob_5 = {
		'top':-60,
		'left':-380,
		'show':'<div class="shine_blu mini-but-s"></div>',
		'showtext':{'text':'Помощь<div class="arrow-text-top"></div>','left':0,'top':-20},
		'type':'help',
		'src':'img/control_info_help_6.png',
		'rotary':false,
		'position': {'top':-800,'left':-215},
		'border':'none'
	}
	
var objectsButton = {
	'ob_1':ob_1,
	'ob_2':ob_2,
	'ob_3':ob_3,
	'ob_4':ob_4,
	'ob_5':ob_5,
}
	
var windowCreate = {
	'window_1':{
		'top':0,
		'left':0,
		'objects':objectsButton
	},
	'window_2':{
		'top':0,
		'left':959,
		'objects':objectsButton
	},
	'window_3':{
		'top':539,
		'left':0,
		'objects':objectsButton
	},
	'window_4':{
		'top':539,
		'left':959,
		'objects':objectsButton
	}
};



