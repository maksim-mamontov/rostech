$(function() {
	
if (createPage) {create_page();} // создаем окна и кнопки на странице
disable_touch(); // отключим все стандартные действия тачьскрина

if (rotaryL){rotaryLine = $(windo)[0].offsetHeight/2;} // получим линию переворота блоков
if (commerc) {commercial();} // функция рекламы

// Админ окно управления
if (adminControlWindow) {admin_control_panel();}

if (rotaryRevers){
	var buff = rotaryHoriz;
	rotaryHoriz = rotaryNone;
	rotaryNone = buff;
}	
	// дописываем активный класс если кликнули по блоку
	$(windo).on('touchstart mousedown',block,function(event) { 
		$(this).addClass(blockActive.substr(1));	});
	$(windo).on('touchend mouseup',block,function(event) { 
		$(this).removeClass(blockActive.substr(1));
	});

	// touch block изменение размеров блока
	$(windo).on('touchstart',blockResize,function(event) { // на все блоки blockTarget повешаем эвент (даже если их нету)
		var timeStart = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var sElx = blockElem.position().left; // расположение ЛЕФТ элемента
		var sEly = blockElem.position().top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(),contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}
		
		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'touch'
		};
		
		if (tt == 2) {
			var touchSA = event.targetTouches[0];
			var touchSB = event.targetTouches[1];
			data['sx'] = Math.round(Math.abs(touchSA.pageX - touchSB.pageX));
			data['sy'] = Math.round(Math.abs(touchSA.pageY - touchSB.pageY));
			
			resize_block(data);
		}
	});
	
	// движение блока
	$(windo).on('touchstart',blockTarget,function(event) { // на все блоки blockTarget повешаем эвент (даже если их нету)
		var timeStart = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var sElx = blockElem.position().left; // расположение ЛЕФТ элемента
		var sEly = blockElem.position().top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(),contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}

		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'touch'
		};
		
		if (tt == 1) {
			if (control2C != 1) {//двойной клик
				var GlobObject = gDataInfo['block_'+index];
				if (GlobObject){
					if (GlobObject.startTime+timerDB >= timeStart) {//двойной клик произашел
						/*
						var heightTE = $(elem).css('height');
						if (heightTE == 40+'px'){
							$(elem).animate({"height": 100+'%'},timerAnimBut);
							$(elem).children(targetOpen).html('&uarr;');
						}
						else {
							$(elem).animate({"height": 40+'px'},timerAnimBut);
							$(elem).children(targetOpen).html('&darr;');
						}
						*/
					}
					gDataInfo['block_'+index] = {'startTime':timeStart};
				}
				else {gDataInfo['block_'+index] = {'startTime':timeStart};}
			}	
			
			var touch = event.targetTouches[0];
			data['sx'] = touch.pageX;
			data['sy'] = touch.pageY;
			
			drag_block(data);
		}
	}),false;
	
	// движение иметирующее скрол, и свайп внутри блока 
	$(windo).on('touchstart',conInfoMove,function(event) { 
		var timeControl = new Date().getTime(); // время клика на объект
		var tt = event.targetTouches.length; // колочество пальцев на объекте
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(conInfoWin); // получим окно в котором элемент
		var rotat = detect_rotate($(this).closest(rotaryBlock).css('transform'));// угол поворота текущего элемента
		// Element Window
		var elemH = elem.height(); // Высота элемента
		//var sEly = elem.position().top; // расположение  ТОП элемент
		var sEly = elem.css('top') // расположение  ТОП элемент
		sEly = Number(sEly.substr(0, sEly.length - 2)); // расположение  ТОП элемент
		var windH = blockElem.height(); // Высота окна
		// переменные для манипуляций
		var diff,y,ys,ye,ty,x,xs,tx,currentTime,lastY=sEly,swipX=false;
		diff = windH-elemH
		
		if (diff <= 0){
		$(elem).off('touchmove');
		var touch = event.targetTouches[0];
		ys = touch.pageY;
		xs = touch.pageX;
			if (rotat == 0){
				$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
					if (event.targetTouches.length == 1) { // 1 касание
						touchm = event.targetTouches[0];
						ty = touchm.pageY-ys;
						tx = touchm.pageX-xs;
						y = sEly+ty;
						if (y < diff) {y = diff;} if (y > 0) {y = 0;}
						if (tx > swipeX3d || tx < -swipeX3d){swipX = true;}
						
						currentTime = new Date().getTime();
						if (timeControl+timerKin <= currentTime) {
							timeControl = currentTime;
							lastY = y;
						}
						
						$(elem).css({'top':y+'px'});
					}
				}),false;					
			}
			if (rotat == 180){
				$(elem).on('touchmove',function(event) { // если тянем 1 пальцем
					if (event.targetTouches.length == 1) { // 1 касание
						touchm = event.targetTouches[0];
						ty = ys-touchm.pageY;
						tx = touchm.pageX-xs;
						y = sEly+ty;
						if (y < diff) {y = diff;} if (y > 0) {y = 0;}
						if (tx > swipeX3d || tx < -swipeX3d){swipX = true;}
						
						currentTime = new Date().getTime();
						if (timeControl+timerKin <= currentTime) {
							timeControl = currentTime;
							lastY = y;
						}
						
						$(elem).css({'top':y+'px'});
						}
				}),false;
			}
			
			if (kinetic){
				$(elem).off('touchend');$(elem).off('mouseup');
				$(elem).one('touchend mouseup',function(event) {
					ye = (y-lastY)*kofDis;
					if (swipX){back_3D(elem);}
					animate({
						duration:timerAnim,
						timing: makeEaseOut(circ),
						draw: function(prog){
							ym = y+(prog*ye);
							if (winFrame){
								if (ym < diff) {ym = diff;} if (ym > 0) {ym = 0;}
							}
							$(elem).css({'top':ym});
						}
					});
				});
			}

		}
	});
	
	
	// mouse
	$(windo).on('mousedown',blockTarget,function(event) {
		var timeStart = new Date().getTime(); // время клика на объект
		var elem = $(this); // получи нажатый элемент
		var blockElem = $(elem).closest(block); // получим основной движемый элемент
		$(blockElem).stop();// остановим анимацию при клике
		var index = $(blockElem).index();
		// Element
		var elW = blockElem.width(); // ширина элемента
		var elH = blockElem.height(); // Высота элемента
		var elXY = blockElem.position();
		var sElx = elXY.left; // расположение ЛЕФТ элемента
		var sEly = elXY.top; // расположение  ТОП элемент
		// Main Window
		var panel = $(blockElem).closest(windo); // получим главное окно
		var Ww = panel.width(); // ширина рамки главного окна
		var Wh = panel.height(); // Высота рамки главного окна
		var data = {};
		// отключим 2й ной клик на блоке
		var control2C = false, datainfo = blockElem.data(), contentBlock; 
		if (datainfo) {control2C = datainfo.control2c;} 
		// проверяем контент блока 
		if (datainfo) {contentBlock = (datainfo.content) ? datainfo.content: 'object'}

		data = {
			'elW':elW,
			'elH':elH,
			'sElx':sElx,
			'sEly':sEly,
			'elem':elem,
			'blockElem':blockElem,
			'Ww':Ww,
			'Wh':Wh,
			'contentBlock':contentBlock,
			'typeEvent':'mouse'
		};
		
		if (control2C != 1) {//двойной клик
			var GlobObject = gDataInfo['block_'+index];
			if (GlobObject){
				if (GlobObject.startTime+timerDB >= timeStart) {// если 2 клик произашел
					/*
					var heightTE = $(elem).css('height');
					if (heightTE == 40+'px'){
						$(elem).animate({"height": 100+'%'},timerAnimBut);
						$(elem).children(targetOpen).html('&uarr;');
					}
					else {
						$(elem).animate({"height": 40+'px'},timerAnimBut);
						$(elem).children(targetOpen).html('&darr;');
					}
					*/
				}
				gDataInfo['block_'+index] = {'startTime':timeStart};
			}
			else {gDataInfo['block_'+index] = {'startTime':timeStart};}
		}	
		
		data['sx'] = event.clientX;
		data['sy'] = event.clientY;

		drag_block(data);
	});
	
	
	// Delete block
	// функция удаления блоков со страници, если по классу blockDelete был клик то удалит по умолчанию весь block со страници
	// но если у класса blockDelete есть data-delet="класс удоляемого объекта" удалит объект с классом из data-delet, если он родитель или выше 
	$(windo).on('click touchstart',blockDelete,function(event) {
		var dataDel = $(this).data();
		
		var elemDel = $(this).closest(block);
		if (dataDel.delet) {elemDel = $(this).closest('.'+dataDel.delet);}
		var elemId3d = $(elemDel).find(canvas3d).attr("id");
		
		if (elemId3d) { // если ИД 3д существует, сначала выгружаем 3д 
			b4w.require(elemId3d,elemId3d).unload_cb();
		}
		elemDel.remove(); // удоляем элемент со страници
	});
	
	// clouse dop info in 3D. закрывает дополнительный контент внутри 3д
	$(windo).on('click touchstart',back3d,function(){
		back_3D(this);
	});
	
	
	// Open Control Menu 
	$(windo).on('click touchstart',targetOpen,function(event) {
		var glBlock = $(this).closest(block); // получим основной блок
		var resizeBlock = $(glBlock).find(blockOpen); // блок который открываем
		var heightReBlock = $(resizeBlock)[0].offsetHeight; // высота блока
		var heigWiReBlock = $(resizeBlock)[0].offsetWidth; // ширина блока
		if (typeOpen == "top") {
			if (heightReBlock > 0){
				animate({
					duration:timerAnimBut,
					draw: function(prog){resizeBlock[0].style.height = 100*(1-prog)+'%';}
				});
				$(this).html(arreyOpen);
			}
			else {
				animate({
					duration:timerAnimBut,
					draw: function(prog){resizeBlock[0].style.height = 100*prog+'%';}
				});
				$(this).html(arreyClos);
			}
		}
		if (typeOpen == "right") {
			if (heigWiReBlock > 0){
				animate({
					duration:timerAnimBut,
					draw: function(prog){resizeBlock[0].style.width = 100*(1-prog)+'%';}
				});
				$(this).html(arreyOpen);
				$(this).css({'color':'#fff'});
			}
			else {
				animate({
					duration:timerAnimBut,
					draw: function(prog){resizeBlock[0].style.width = 100*prog+'%';}
				});
				$(this).html(arreyClos);
				$(this).css({'color':'#f00'});
			}
		}

	});
	
	
	// mini button click создаем блок на поле
	$(butСС).on('click touchstart', function(event) {
		var elem = $(this);
		add_new_block(elem);// функция создания нового блока на поле
	});
	
	// при клике на неактивный блок таргет (визуализация)
	$(windo).on('mousedown touchstart',blockTargetV,function(event) {
		$(this).addClass(blockTargetVA.substr(1));
		$(this).on('touchend mouseup',function(event){
			$(this).removeClass(blockTargetVA.substr(1));
		});
	});

	// background buttun 
	$(butBС).on('click touchstart', function(event) {
		var cloneInfo = $(this).find(cloneContent);
		var dataInfo = cloneInfo.data();
		var target = $(this).closest(windowControl)
		var windowTop = target.css('top');
		var windowLeft = target.css('left');		
		if (dataInfo.type) {
		var bacSrc = dataInfo.src;
			if (dataInfo.type == 'background') {
				var background = $(target).css('background-image');
				if ( $(target).css('background-image') != 'none' ){
					$(target).css({'background-image':'none'});
				}
				else {
					$(target).css({'background-image':'url('+bacSrc+')'});
				}
			}
		}
	});
	$(butBBС).on('click touchstart', function(event) {
		var target = $(this).closest(clCenMinBut)
		var cloneInfo = $(target).find(cloneContent);
		var dataInfo = cloneInfo.data();
		var target = $(target).closest(windowControl)
		
		if (dataInfo.type) {
		var bacSrc = dataInfo.src;
			if (dataInfo.type == 'background') {
				var background = $(target).css('background-image');
				if ( $(target).css('background-image') != 'none' ){
					$(target).css({'background-image':'none'});
				}
				else {
					$(target).css({'background-image':'url('+bacSrc+')'});
				}
			}
		}
	});
	
	// Help buttun 
	// при клике включить окно помощи
	$(butHelpWind).on('click touchstart', function(event) {
		var target = $(this).closest(windowControl)
		var targetId = target.attr('id');
		var windowW = target.css('width');
		var windowH = target.css('height');
		var leftH = (target[0].offsetWidth-helpWindowWidth)/2;
		var helpWindow = $('#help_'+targetId);
		var helpWindowCont = helpWindow.find(HelpWindContent);
		var helpWindowClose = helpWindow.find(butHelpCloWind);

		helpWindow.css({'width':windowW,'height':windowH});
		helpWindowClose.css({'display':'none'});
		if (HelpWindViewContentPos = 'bot') {
			helpWindowCont.css({'width':'100%','height':'100%','opacity':'0.0'});
		}
		helpWindowCont.animate({'opacity':'1.0'},HelpWindContentTime,function(){
			helpWindowClose.css({'display':'block'});
		});
	});
	// при клике на кнопку удаления хелп окна
	$(windo).on('mousedown touchstart',butHelpCloWind,function(event) {
		$(this).css({'display':'none'});
		var tarHelpWind = $(this).closest(HelpWind);
		var helpWindowCont = tarHelpWind.find(HelpWindContent);
		helpWindowCont.animate({'opacity':'0.0'},HelpWindContentTimeShad,function(){
			tarHelpWind.css({'width':'0px','height':'0px'});
		});
	});
	
	// если включено нажатие на всплывающий текст
	if (showDopInfTextClick) {
		$(showDopInfText).on('click touchstart',function(){
			var elem = $(this).parent().find(butСС);
			add_new_block(elem);
		});
	}
	
	// нажатие на центральную кнопку
	$(cenButCli).on('click touchstart', function(event) {
		var timeStart = new Date().getTime(); // время клика на объект
		var elem = $(this).closest(centralBut);
		remove_button(elem);
	});
	
	// функция анализирующая активно окно или нет, поднимает кнопку если окно неактивно
	if (returnButton) {ad_window();}
		
	// если пользователь 1 то переходим в активацию вип режима
	if (allWindowCo == 1) {
		var VIPbias = 'left: calc(-'+VIPbiasBut+'% - 25px);';
		var rotateButVip = '<div class="'+vipRotateBut.substr(1)+' '+vipRotateButDop.substr(1)+'" style="'+VIPbias+';"><div class="shine mini-but-s"></div><img src="img/icon/rotate-arrow_mini.png"></div>'
		$(windowControl).append(rotateButVip);
		
		// Вип кнопка переворота экрана
		$(vipRotateBut).on('click touchstart',function(){
			var elem = this;
			var windowVip = $(elem).closest(windowControl);
			var windowVipRotat = detect_rotate($(windowVip).css('transform'));
			var VIPbiasButRev = 100-VIPbiasBut;
			var VIPhelpDias = HWVSizW/2;
			
			if (windowVipRotat == 0) {
				$(windowControl).css({'transform':'rotate(180deg)'});
				$(rotaryBlock).css({'transform':'rotate(180deg)'});
				$(centralBut).css({'left':'calc('+VIPbiasButRev+'% - 25px)'});
				$(vipRotateButDop).css({'left':'calc('+VIPbiasBut+'% - 25px)'});
				$(HelpWind).css({'transform':'rotate(180deg)'});
				$(HelpWindViewContentSiz).css({'left':'calc('+VIPbiasButRev+'% - '+VIPhelpDias+'px)'});
			}
			
			if (windowVipRotat == 180) {
				$(windowControl).css({'transform':'rotate(0deg)'});
				$(rotaryBlock).css({'transform':'rotate(0deg)'});
				$(centralBut).css({'left':'calc('+VIPbiasBut+'% - 25px)'});
				$(vipRotateButDop).css({'left':'calc(-'+VIPbiasBut+'% - 25px)'});
				$(HelpWind).css({'transform':'rotate(0deg)'});
				$(HelpWindViewContentSiz).css({'left':'calc('+VIPbiasBut+'% - '+VIPhelpDias+'px)'});
			}
			
		});
	}
	
	
// MObile Version
var id3window = 'canvas3d_2t';
	//Click General button
	$('.ttm-central-but').on('click touchstart', function(){
		var timeStart = new Date().getTime(); // время клика на объект
		var elem = $(this);
		var elemMenu = $('.ttm-all-mini-but');
		var rotatBut = detect_rotate(elem.css('transform'));
		if (!gDataInfo.mobileCentralButTime) {gDataInfo.mobileCentralButTime=0;}
		var timerTest = timeStart-gDataInfo.mobileCentralButTime;
		var animRotate = -90;
		var timerAnimMob = 500; // время анимации
		var timerDelayMob = 1000;// задержка между кликами
		
		if (timerTest > timerDelayMob){ // задержка между нажатиями на кнопку
			gDataInfo.mobileCentralButTime = timeStart;
			if (rotatBut == 0) {
				animate({
					duration:timerAnimMob,
					draw: function(prog){
						var anim = animRotate*prog;
						var animM = 100*prog;
						
						elemMenu.css({'width':animM+'%','opacity':prog});
						elem.css({'transform':'rotate('+anim+'deg)'});
					}
				});
			}else {
				animate({
					duration:timerAnimMob,
					draw: function(prog){
						prog = 1-prog
						var animM = 100*prog;
						var anim = animRotate*prog;
						
						elemMenu.css({'width':animM+'%','opacity':prog});
						elem.css({'transform':'rotate('+anim+'deg)'});
					}
				});
			}
		}
		else {}
	});
	
	// Click mini button
	$('.ttm-mini-but').on('click touchstart', function(){
		var elem = $(this);
		var url3dcontent = elem.data('cont3d');
		
		b4w.require(id3window,id3window).unload_cb();

		$('.preloader3d').css({'display':'block'});
		b4w.require(id3window,id3window).load_3d(url3dcontent);
		console.log(elem.data('cont3d'));
	});
	
	
});

